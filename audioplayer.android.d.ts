import * as nsApp from '@nativescript/core/application';
import { CommonAudioPlayer } from './audioplayer-common';
import { MediaTrackData, Playlist } from './audioplayer.types';
import './media-service';
export declare class TNSAudioPlayer extends CommonAudioPlayer {
    protected readonly cls: string;
    private get context();
    private _mediaServicePromise?;
    private _mediaServiceResolve;
    private _mediaServiceReject;
    private _mediaService?;
    private get mediaService();
    private _serviceConnection;
    get android(): dk.nota.MediaService | undefined;
    preparePlaylist(playlist: Playlist): Promise<void>;
    getCurrentPlaylistIndex(): Promise<number>;
    play(): Promise<void>;
    pause(): Promise<void>;
    stop(): Promise<void>;
    isPlaying(): Promise<boolean>;
    seekTo(offset: number): Promise<void>;
    changeMediaTrackData(data: MediaTrackData): Promise<void>;
    skipToPlaylistIndexAndOffset(playlistIndex: number, offset: number): Promise<void>;
    skipToPrevious(): Promise<void>;
    skipToNext(): Promise<void>;
    skipToPlaylistIndex(playlistIndex: number): Promise<void>;
    setRate(rate: number): Promise<void>;
    getRate(): Promise<number>;
    getDuration(): Promise<number>;
    getCurrentTime(): Promise<number>;
    setSeekIntervalSeconds(seconds: number): Promise<void>;
    destroy(): void;
    private startMediaService;
    private stopMediaService;
    protected _exitHandler(args: nsApp.ApplicationEventData): void;
}
