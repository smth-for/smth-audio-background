"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var nsApp = require("@nativescript/core/application");
var image_source_1 = require("@nativescript/core/image-source");
var trace = require("@nativescript/core/trace");
var nsUtils = require("@nativescript/core/utils/utils");
var audioplayer_types_1 = require("./audioplayer.types");
var MEDIA_SERVICE_NAME = 'TNS-MediaService-1';
var DEFAULT_PLAYBACK_RATE = 1;
var DEFAULT_INTENT_CODE = 1337;
var DEFAULT_SEEK_LENGTH = 15;
var dk;
(function (dk) {
    var nota;
    (function (nota) {
        var instance = 0;
        var MediaService = (function (_super) {
            __extends(MediaService, _super);
            function MediaService() {
                var _this = _super.call(this) || this;
                _this._rate = DEFAULT_PLAYBACK_RATE;
                _this._seekIntervalSeconds = DEFAULT_SEEK_LENGTH;
                _this._intentReqCode = DEFAULT_INTENT_CODE;
                return global.__native(_this);
            }
            MediaService_1 = MediaService;
            Object.defineProperty(MediaService.prototype, "cls", {
                get: function () {
                    if (!this._cls) {
                        this._cls = "MediaService<" + ++instance + ">";
                    }
                    return this._cls;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MediaService.prototype, "_sessionToken", {
                get: function () {
                    var _a;
                    return (_a = this._mediaSession) === null || _a === void 0 ? void 0 : _a.getSessionToken();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MediaService.prototype, "owner", {
                get: function () {
                    var _a, _b;
                    return _b = (_a = this._owner) === null || _a === void 0 ? void 0 : _a.get(), (_b !== null && _b !== void 0 ? _b : null);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MediaService.prototype, "albumArts", {
                get: function () {
                    if (!this._albumArts) {
                        this._albumArts = new Map();
                    }
                    return this._albumArts;
                },
                enumerable: true,
                configurable: true
            });
            MediaService.prototype.onCreate = function () {
                var _this = this;
                var _a, _b, _c, _d;
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".onCreate()", audioplayer_types_1.notaAudioCategory);
                }
                _super.prototype.onCreate.call(this);
                var appContext = this.getApplicationContext();
                ensureNativeClasses();
                var sessionIntent = (_a = this.getPackageManager()) === null || _a === void 0 ? void 0 : _a.getLaunchIntentForPackage(this.getPackageName());
                var sessionActivityPendingIntent = null;
                if (sessionIntent) {
                    sessionActivityPendingIntent = android.app.PendingIntent.getActivity(this, 0, sessionIntent, 0);
                }
                this._rate = DEFAULT_PLAYBACK_RATE;
                this._seekIntervalSeconds = DEFAULT_SEEK_LENGTH;
                this._intentReqCode = DEFAULT_INTENT_CODE;
                this._isForegroundService = false;
                var trackSelector = new com.google.android.exoplayer2.trackselection.DefaultTrackSelector(this);
                var loadControl = new com.google.android.exoplayer2.DefaultLoadControl.Builder()
                    .setBufferDurationsMs(1000 * 60, 1000 * 60 * 10, com.google.android.exoplayer2.DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_MS, com.google.android.exoplayer2.DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS)
                    .createDefaultLoadControl();
                var playerListener = new TNSPlayerEvent(this);
                this._mediaSession = new android.support.v4.media.session.MediaSessionCompat(this, MEDIA_SERVICE_NAME);
                if (sessionActivityPendingIntent) {
                    this._mediaSession.setSessionActivity(sessionActivityPendingIntent);
                }
                this._mediaSession.setActive(true);
                this._mediaSession.setFlags(android.support.v4.media.session.MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                    android.support.v4.media.session.MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
                this._mediaSession.setMediaButtonReceiver(null);
                this._mediaSessionConnector = new com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector(this._mediaSession);
                this._mediaSessionConnector.setEnabledPlaybackActions(android.support.v4.media.session.PlaybackStateCompat.ACTION_PLAY |
                    android.support.v4.media.session.PlaybackStateCompat.ACTION_PLAY_PAUSE |
                    android.support.v4.media.session.PlaybackStateCompat.ACTION_PAUSE);
                this._mediaSessionMetadataProvider = new com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.MediaMetadataProvider({
                    getMetadata: function () {
                        var _a, _b, _c, _d, _e, _f;
                        var info = _this._getTrackInfo((_b = (_a = _this.exoPlayer) === null || _a === void 0 ? void 0 : _a.getCurrentWindowIndex(), (_b !== null && _b !== void 0 ? _b : 0)));
                        return new android.support.v4.media.MediaMetadataCompat.Builder()
                            .putString(android.support.v4.media.MediaMetadataCompat.METADATA_KEY_TITLE, (_d = (_c = info) === null || _c === void 0 ? void 0 : _c.title, (_d !== null && _d !== void 0 ? _d : 'Unknown')))
                            .putString(android.support.v4.media.MediaMetadataCompat.METADATA_KEY_ARTIST, (_f = (_e = info) === null || _e === void 0 ? void 0 : _e.artist, (_f !== null && _f !== void 0 ? _f : 'Unknown')))
                            .build();
                    },
                });
                this._mediaSessionConnector.setMediaMetadataProvider(this._mediaSessionMetadataProvider);
                var notificationTitle = (_b = nsUtils.ad.resources.getStringId('tns_audioplayer_notification_title'), (_b !== null && _b !== void 0 ? _b : android.R.string.unknownName));
                var notificationDesc = (_c = nsUtils.ad.resources.getStringId('tns_audioplayer_notification_desc'), (_c !== null && _c !== void 0 ? _c : android.R.string.unknownName));
                this._playerNotificationManager = com.google.android.exoplayer2.ui.PlayerNotificationManager.createWithNotificationChannel(this, MEDIA_SERVICE_NAME, notificationTitle, notificationDesc, this._intentReqCode, new TNSMediaDescriptionAdapter(this), new TNSNotificationListener(this));
                var exoPlayer = new com.google.android.exoplayer2.SimpleExoPlayer.Builder(appContext)
                    .setTrackSelector(trackSelector)
                    .setLoadControl(loadControl)
                    .build();
                exoPlayer.setHandleWakeLock(true);
                exoPlayer.setHandleAudioBecomingNoisy(true);
                exoPlayer.addListener(playerListener);
                var metadata = new TNSPlayerMetadata(this);
                exoPlayer.addMetadataOutput(metadata);
                this._playerNotificationManager.setMediaSessionToken(this._sessionToken);
                this._playerNotificationManager.setUseChronometer(true);
                (_d = this._mediaSessionConnector) === null || _d === void 0 ? void 0 : _d.setPlayer(exoPlayer);
                this._albumArts = new Map();
                var audioAttributes = new com.google.android.exoplayer2.audio.AudioAttributes.Builder()
                    .setUsage(com.google.android.exoplayer2.C.USAGE_MEDIA)
                    .setContentType(com.google.android.exoplayer2.C.CONTENT_TYPE_MUSIC)
                    .build();
                exoPlayer.getAudioComponent().setAudioAttributes(audioAttributes, true);
                this.exoPlayer = exoPlayer;
            };
            MediaService.prototype._getTrackInfo = function (index) {
                var _a, _b;
                return (_b = (_a = this._playlist) === null || _a === void 0 ? void 0 : _a.tracks) === null || _b === void 0 ? void 0 : _b[index];
            };
            MediaService.prototype._handleTimeChange = function () {
                var _a;
                var player = this.exoPlayer;
                if (!player) {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + "._onPlaying() - no player stop timeChangeInterval", audioplayer_types_1.notaAudioCategory);
                    }
                    clearInterval(this._timeChangeInterval);
                    return;
                }
                var windowIndex = player.getCurrentWindowIndex();
                var position = player.getCurrentPosition();
                var duration = player.getDuration();
                if (this.lastPosition !== position || this.lastWindowIndex !== windowIndex) {
                    (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onTimeChanged(position, duration, windowIndex);
                    this.lastPosition = position;
                    this.lastWindowIndex = windowIndex;
                }
            };
            MediaService.prototype._onPlaying = function () {
                var _this = this;
                var _a, _b;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onPlaying()", audioplayer_types_1.notaAudioCategory);
                }
                clearInterval(this._timeChangeInterval);
                this._timeChangeInterval = setInterval(function () {
                    _this._handleTimeChange();
                }, 100);
                (_a = this._mediaSession) === null || _a === void 0 ? void 0 : _a.setActive(true);
                this._handleTimeChange();
                (_b = this.owner) === null || _b === void 0 ? void 0 : _b._onPlaying();
            };
            MediaService.prototype._onPaused = function () {
                var _a;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onPaused()", audioplayer_types_1.notaAudioCategory);
                }
                clearInterval(this._timeChangeInterval);
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onPaused();
            };
            MediaService.prototype._onStopped = function () {
                var _a, _b, _c;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onStopped()", audioplayer_types_1.notaAudioCategory);
                }
                clearInterval(this._timeChangeInterval);
                (_a = this._mediaSession) === null || _a === void 0 ? void 0 : _a.setActive(false);
                (_b = this._mediaSessionConnector) === null || _b === void 0 ? void 0 : _b.setPlayer(null);
                (_c = this.owner) === null || _c === void 0 ? void 0 : _c._onStopped();
            };
            MediaService.prototype._onEndOfPlaylistReached = function () {
                var _a;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onEndOfPlaylistReached()", audioplayer_types_1.notaAudioCategory);
                }
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onEndOfPlaylistReached();
            };
            MediaService.prototype._onEndOfTrackReached = function () {
                var _a;
                if (this.exoPlayer) {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + "._onEndOfTrackReached()", audioplayer_types_1.notaAudioCategory);
                    }
                    var endedTrackIndex = this.exoPlayer.getCurrentWindowIndex();
                    (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onEndOfTrackReached(endedTrackIndex);
                }
                else {
                    trace.write(this.cls + "._onEndOfTrackReached() - exoPlayer not initialized", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                }
            };
            MediaService.prototype._onBuffering = function () {
                var _a;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onBuffering()", audioplayer_types_1.notaAudioCategory);
                }
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onBuffering();
            };
            MediaService.prototype._onMetadataReceived = function (title, name, genre) {
                if (name === void 0) { name = ''; }
                if (genre === void 0) { genre = ''; }
                var _a;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onMetadataReceived()", audioplayer_types_1.notaAudioCategory);
                }
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onMetadataReceived(title, name, genre);
            };
            MediaService.prototype._onWaitingForNetwork = function () {
                var _a;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onWaitingForNetwork()", audioplayer_types_1.notaAudioCategory);
                }
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onWaitingForNetwork();
            };
            MediaService.prototype._onPlaybackError = function (errorData) {
                var _a;
                trace.write(this.cls + "._onPlaybackError(" + errorData + ")", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onPlaybackError(errorData);
            };
            MediaService.prototype._onPlaybackSuspend = function (reason) {
                var _a;
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._onPlaybackSuspend(" + reason + ")", audioplayer_types_1.notaAudioCategory);
                }
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onPlaybackSuspended(reason);
            };
            MediaService.prototype.onDestroy = function () {
                var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".onDestroy()", audioplayer_types_1.notaAudioCategory);
                }
                this.stopForeground(true);
                if (this._isForegroundService) {
                    this._isForegroundService = false;
                }
                if (this.isPlaying()) {
                    (_a = this.exoPlayer) === null || _a === void 0 ? void 0 : _a.stop();
                }
                (_b = this._playerNotificationManager) === null || _b === void 0 ? void 0 : _b.setMediaSessionToken(null);
                (_c = this._playerNotificationManager) === null || _c === void 0 ? void 0 : _c.setPlaybackPreparer(null);
                (_d = this._playerNotificationManager) === null || _d === void 0 ? void 0 : _d.setControlDispatcher(null);
                (_e = this._playerNotificationManager) === null || _e === void 0 ? void 0 : _e.setPlayer(null);
                delete this._playerNotificationManager;
                (_f = this._mediaSessionConnector) === null || _f === void 0 ? void 0 : _f.setPlayer(null);
                (_g = this._mediaSessionConnector) === null || _g === void 0 ? void 0 : _g.setMediaMetadataProvider(null);
                delete this._mediaSessionConnector;
                delete this._mediaSessionMetadataProvider;
                (_h = this._mediaSession) === null || _h === void 0 ? void 0 : _h.setActive(false);
                (_j = this._mediaSession) === null || _j === void 0 ? void 0 : _j.release();
                delete this._mediaSession;
                (_k = this.exoPlayer) === null || _k === void 0 ? void 0 : _k.release();
                delete this.exoPlayer;
                clearInterval(this._timeChangeInterval);
                delete this._owner;
                (_l = this._albumArts) === null || _l === void 0 ? void 0 : _l.clear();
                delete this._albumArts;
                delete this._playlist;
                delete this._lastLoadedAlbumArt;
                _super.prototype.onDestroy.call(this);
            };
            MediaService.prototype._handleNotificationPosted = function (notificationId, notification) {
                if (!this.exoPlayer) {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + "._handleNotificationPosted(" + notificationId + ", " + notification + ") - missing exoplayer", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    }
                    return;
                }
                var playerState = this.exoPlayer.getPlaybackState();
                var isBuffering = playerState === com.google.android.exoplayer2.Player.STATE_BUFFERING;
                var shouldBeInForeground = isBuffering || this.isPlaying();
                if (trace.isEnabled()) {
                    trace.write(this.cls + "._handleNotificationPosted(" + notificationId + ", " + notification + ") - should be in:" + shouldBeInForeground + ". is in foreground:" + this._isForegroundService, audioplayer_types_1.notaAudioCategory);
                }
                if (shouldBeInForeground === this._isForegroundService) {
                    return;
                }
                if (shouldBeInForeground) {
                    androidx.core.content.ContextCompat.startForegroundService(this.getApplicationContext(), new android.content.Intent(this, dk.nota.MediaService.class));
                    this.startForeground(notificationId, notification);
                    this._isForegroundService = true;
                    if (trace.isEnabled()) {
                        trace.write(this.cls + "._handleNotificationPosted(" + notificationId + ", " + notification + ") - started in foreground", audioplayer_types_1.notaAudioCategory);
                    }
                }
                else {
                    var shouldStopSelf = playerState === com.google.android.exoplayer2.Player.STATE_ENDED;
                    this.stopForeground(shouldStopSelf);
                    this._isForegroundService = false;
                    if (shouldStopSelf) {
                        this.stopSelf();
                    }
                    if (trace.isEnabled()) {
                        trace.write(this.cls + "._handleNotificationPosted(" + notificationId + ", " + notification + ") - stopped in foreground. stopSelf:" + isBuffering, audioplayer_types_1.notaAudioCategory);
                    }
                }
            };
            MediaService.prototype.onBind = function (intent) {
                var action = "" + intent.getAction();
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".onBind(" + intent + ") action: " + action, audioplayer_types_1.notaAudioCategory);
                }
                return new MediaService_1.LocalBinder(this);
            };
            MediaService.prototype.onStartCommand = function (intent, flags, startId) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".onStartCommand(" + intent + ", " + flags + ", " + startId + ") - " + this._mediaSession, audioplayer_types_1.notaAudioCategory);
                }
                if (this._mediaSession) {
                    TNSMediaButtonReceiver.handleIntent(this._mediaSession, intent);
                }
                return _super.prototype.onStartCommand.call(this, intent, flags, startId);
            };
            MediaService.prototype.setOwner = function (owner) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".setOwner()", audioplayer_types_1.notaAudioCategory);
                }
                if (owner) {
                    this._owner = new WeakRef(owner);
                }
                else {
                    this._owner = undefined;
                }
            };
            MediaService.prototype.preparePlaylist = function (playlist) {
                var _a;
                return __awaiter(this, void 0, void 0, function () {
                    var concatenatedSource, userAgent, mediaSourceFactory, _b, _c, track, mediaSource, _d, notificationIcon;
                    var e_1, _e;
                    return __generator(this, function (_f) {
                        switch (_f.label) {
                            case 0:
                                if (!this.exoPlayer || !this._playerNotificationManager) {
                                    trace.write(this.cls + ".preparePlaylist() - exoPlayer not initialized", audioplayer_types_1.notaAudioCategory);
                                    return [2];
                                }
                                if (trace.isEnabled()) {
                                    trace.write(this.cls + ".preparePlaylist()", audioplayer_types_1.notaAudioCategory);
                                }
                                this.exoPlayer.stop();
                                concatenatedSource = new com.google.android.exoplayer2.source.ConcatenatingMediaSource(Array.create(com.google.android.exoplayer2.source.ExtractorMediaSource, 0));
                                userAgent = com.google.android.exoplayer2.util.Util.getUserAgent(this, 'tns-audioplayer');
                                mediaSourceFactory = new com.google.android.exoplayer2.source.ProgressiveMediaSource.Factory(new com.google.android.exoplayer2.upstream.DefaultDataSourceFactory(this, new com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory(userAgent, com.google.android.exoplayer2.upstream.DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, 30 * 1000, true)));
                                this.albumArts.clear();
                                try {
                                    for (_b = __values(playlist.tracks), _c = _b.next(); !_c.done; _c = _b.next()) {
                                        track = _c.value;
                                        if (!this._checkUrlAllowed(track.url)) {
                                            trace.write(this.cls + ".preparePlaylist() - clear text traffic is not allowed - \"" + track.url + "\"", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                                        }
                                        mediaSource = mediaSourceFactory.createMediaSource(android.net.Uri.parse(track.url));
                                        concatenatedSource.addMediaSource(mediaSource);
                                        if (track.albumArtUrl != null) {
                                            this._makeAlbumArtImageSource(track.albumArtUrl);
                                        }
                                    }
                                }
                                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                                finally {
                                    try {
                                        if (_c && !_c.done && (_e = _b.return)) _e.call(_b);
                                    }
                                    finally { if (e_1) throw e_1.error; }
                                }
                                if (!(this.albumArts.size > 0)) return [3, 4];
                                _f.label = 1;
                            case 1:
                                _f.trys.push([1, 3, , 4]);
                                return [4, Promise.all(__spread(this.albumArts.values()))];
                            case 2:
                                _f.sent();
                                return [3, 4];
                            case 3:
                                _d = _f.sent();
                                return [3, 4];
                            case 4:
                                this._playerNotificationManager.setPlayer(this.exoPlayer);
                                this._playerNotificationManager.setVisibility(androidx.core.app.NotificationCompat.VISIBILITY_PUBLIC);
                                this._playerNotificationManager.setUseNavigationActionsInCompactView(true);
                                this._playerNotificationManager.setUsePlayPauseActions(true);
                                this._playerNotificationManager.setUseNavigationActions(false);
                                this._playerNotificationManager.setUseStopAction(false);
                                notificationIcon = nsUtils.ad.resources.getDrawableId('tns_audioplayer_small_icon');
                                if (notificationIcon) {
                                    this._playerNotificationManager.setSmallIcon(notificationIcon);
                                }
                                this._playlist = playlist;
                                this.setRate(this._rate);
                                this.setSeekIntervalSeconds(this._seekIntervalSeconds);
                                (_a = this._mediaSessionConnector) === null || _a === void 0 ? void 0 : _a.setPlayer(this.exoPlayer);
                                this.exoPlayer.prepare(concatenatedSource);
                                return [2];
                        }
                    });
                });
            };
            MediaService.prototype.setSeekIntervalSeconds = function (seconds) {
                if (!this._playerNotificationManager) {
                    trace.write(this.cls + ".setSeekIntervalSeconds(" + seconds + ") - player notification missing", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    return;
                }
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".setSeekIntervalSeconds(" + seconds + ")", audioplayer_types_1.notaAudioCategory);
                }
                this._seekIntervalSeconds = Math.max((seconds !== null && seconds !== void 0 ? seconds : DEFAULT_SEEK_LENGTH), DEFAULT_SEEK_LENGTH);
                var seekMs = this._seekIntervalSeconds * 1000;
                this._playerNotificationManager.setFastForwardIncrementMs(seekMs);
                this._playerNotificationManager.setRewindIncrementMs(seekMs);
            };
            MediaService.prototype.setRate = function (rate) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".setRate(" + rate + ")", audioplayer_types_1.notaAudioCategory);
                }
                if (typeof rate !== 'number' || Number.isNaN(rate) || rate <= 0) {
                    trace.write(this.cls + ".setRate(" + rate + ") - " + JSON.stringify(rate) + " isn't a valid value, setting to 1", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    rate = 1;
                }
                this._rate = rate;
                if (!this.exoPlayer) {
                    trace.write(this.cls + ".setRate(" + rate + ")", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    return;
                }
                var params = new com.google.android.exoplayer2.PlaybackParameters(rate);
                this.exoPlayer.setPlaybackParameters(params);
            };
            MediaService.prototype.getRate = function () {
                var _a, _b, _c;
                if (!this.exoPlayer) {
                    trace.write(this.cls + ".getRate() - exoPlayer not initialized", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    return this._rate;
                }
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".getRate()", audioplayer_types_1.notaAudioCategory);
                }
                return _c = (_b = (_a = this.exoPlayer.getPlaybackParameters()) === null || _a === void 0 ? void 0 : _a.speed, (_b !== null && _b !== void 0 ? _b : this._rate)), (_c !== null && _c !== void 0 ? _c : DEFAULT_PLAYBACK_RATE);
            };
            MediaService.prototype.isPlaying = function () {
                if (!this.exoPlayer) {
                    trace.write(this.cls + ".isPlaying() - exoPlayer not initialized", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    return false;
                }
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".isPlaying()", audioplayer_types_1.notaAudioCategory);
                }
                return !!this.exoPlayer.isPlaying();
            };
            MediaService.prototype.updateCurrentTrackData = function (data) {
                var _a, _b;
                var track = this._getTrackInfo((_b = (_a = this.exoPlayer) === null || _a === void 0 ? void 0 : _a.getCurrentWindowIndex(), (_b !== null && _b !== void 0 ? _b : 0)));
                if (this._playerNotificationManager &&
                    track &&
                    track.title !== data.title &&
                    track.album !== data.album &&
                    track.artist !== data.artist &&
                    track.albumArtUrl !== data.albumArtUrl) {
                    track.title = data.title;
                    track.album = data.album;
                    track.artist = data.artist;
                    track.albumArtUrl = data.albumArtUrl;
                    this._playerNotificationManager.invalidate();
                }
            };
            MediaService.prototype.play = function () {
                if (!this.exoPlayer) {
                    trace.write(this.cls + ".play() - exoPlayer not initialized", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    return;
                }
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".play()", audioplayer_types_1.notaAudioCategory);
                }
                this.exoPlayer.setPlayWhenReady(true);
            };
            MediaService.prototype.pause = function () {
                if (!this.exoPlayer) {
                    trace.write(this.cls + ".pause() - exoPlayer not initialized", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    return;
                }
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".pause()", audioplayer_types_1.notaAudioCategory);
                }
                this.exoPlayer.setPlayWhenReady(false);
            };
            MediaService.prototype.stop = function () {
                if (!this.exoPlayer) {
                    trace.write(this.cls + ".stop() - exoPlayer not initialized", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    return;
                }
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".stop()", audioplayer_types_1.notaAudioCategory);
                }
                this.exoPlayer.stop();
                this.albumArts.clear();
                this._playlist = undefined;
            };
            MediaService.prototype.onTaskRemoved = function (rootIntent) {
                var _a;
                _super.prototype.onTaskRemoved.call(this, rootIntent);
                (_a = this.exoPlayer) === null || _a === void 0 ? void 0 : _a.stop(true);
            };
            MediaService.prototype._makeAlbumArtImageSource = function (url) {
                if (!this._checkUrlAllowed(url)) {
                    trace.write(this.cls + ".makeAlbumArtImageSource(" + url + ") - clear text traffic not allowed - \"" + url + "\"", audioplayer_types_1.notaAudioCategory);
                    return Promise.reject();
                }
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".makeAlbumArtImageSource(" + url + ")", audioplayer_types_1.notaAudioCategory);
                }
                if (!this.albumArts.has(url)) {
                    this.albumArts.set(url, image_source_1.ImageSource.fromUrl(url));
                }
                return this.albumArts.get(url);
            };
            MediaService.prototype._loadAlbumArt = function (track, callback) {
                var _a, _b;
                return __awaiter(this, void 0, void 0, function () {
                    var start, image, err_1;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                if (!((_a = track) === null || _a === void 0 ? void 0 : _a.albumArtUrl)) {
                                    trace.write(this.cls + "._loadAlbumArt(...) - invalid albumArtUrl", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                                    callback.onBitmap(null);
                                    return [2];
                                }
                                if (trace.isEnabled()) {
                                    trace.write(this.cls + "._loadAlbumArt(...)", audioplayer_types_1.notaAudioCategory);
                                }
                                start = Date.now();
                                _c.label = 1;
                            case 1:
                                _c.trys.push([1, 3, , 4]);
                                return [4, this._makeAlbumArtImageSource(track.albumArtUrl)];
                            case 2:
                                image = _c.sent();
                                if ((_b = image) === null || _b === void 0 ? void 0 : _b.android) {
                                    callback.onBitmap(image.android);
                                    if (trace.isEnabled()) {
                                        trace.write(this.cls + "._loadAlbumArt(" + track.albumArtUrl + ") - loaded in " + (Date.now() - start), audioplayer_types_1.notaAudioCategory);
                                    }
                                    this._lastLoadedAlbumArt = {
                                        url: track.albumArtUrl,
                                        bitmap: image.android,
                                    };
                                    return [2];
                                }
                                else {
                                    if (trace.isEnabled()) {
                                        trace.write(this.cls + "._loadAlbumArt(" + track.albumArtUrl + ") - not loaded", audioplayer_types_1.notaAudioCategory);
                                    }
                                }
                                return [3, 4];
                            case 3:
                                err_1 = _c.sent();
                                trace.write(this.cls + "._loadAlbumArt(" + track.albumArtUrl + ") - couldn't be loaded. " + err_1 + " " + err_1.message, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                                return [3, 4];
                            case 4:
                                callback.onBitmap(null);
                                return [2];
                        }
                    });
                });
            };
            MediaService.prototype._checkUrlAllowed = function (url) {
                var _a;
                if (android.os.Build.VERSION.SDK_INT < 23) {
                    return true;
                }
                var clearTextHostname = (_a = url.match(/^http:\/\/([^\/]+)/)) === null || _a === void 0 ? void 0 : _a[1];
                if (!clearTextHostname) {
                    return true;
                }
                if (android.os.Build.VERSION.SDK_INT === 23) {
                    return !!android.security.NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
                }
                return !!android.security.NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(clearTextHostname);
            };
            var MediaService_1;
            MediaService = MediaService_1 = __decorate([
                JavaProxy('dk.nota.MediaService'),
                __metadata("design:paramtypes", [])
            ], MediaService);
            return MediaService;
        }(android.app.Service));
        nota.MediaService = MediaService;
        var TNSMediaButtonReceiver = (function (_super) {
            __extends(TNSMediaButtonReceiver, _super);
            function TNSMediaButtonReceiver() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TNSMediaButtonReceiver.prototype.onReceive = function (context, intent) {
                if (trace.isEnabled()) {
                    trace.write("TNSMediaButtonReceiver.onReceive() - " + context + " - " + intent, audioplayer_types_1.notaAudioCategory);
                }
                try {
                    _super.prototype.onReceive.call(this, context, intent);
                }
                catch (e) {
                    if (trace.isEnabled()) {
                        trace.write("TNSMediaButtonReceiver.onReceive() - error - " + e, audioplayer_types_1.notaAudioCategory, trace.messageType.warn);
                    }
                }
            };
            TNSMediaButtonReceiver = __decorate([
                JavaProxy('dk.nota.TNSMediaButtonReceiver')
            ], TNSMediaButtonReceiver);
            return TNSMediaButtonReceiver;
        }(androidx.media.session.MediaButtonReceiver));
        nota.TNSMediaButtonReceiver = TNSMediaButtonReceiver;
        (function (MediaService) {
            var LocalBinder = (function (_super) {
                __extends(LocalBinder, _super);
                function LocalBinder(owner) {
                    var _this = _super.call(this) || this;
                    _this.owner = new WeakRef(owner);
                    return global.__native(_this);
                }
                LocalBinder.prototype.getService = function () {
                    var _a;
                    return (_a = this.owner) === null || _a === void 0 ? void 0 : _a.get();
                };
                return LocalBinder;
            }(android.os.Binder));
            MediaService.LocalBinder = LocalBinder;
        })(MediaService = nota.MediaService || (nota.MediaService = {}));
    })(nota = dk.nota || (dk.nota = {}));
})(dk = exports.dk || (exports.dk = {}));
var TNSPlayerEvent;
var TNSPlayerMetadata;
var TNSMediaDescriptionAdapter;
var TNSNotificationListener;
var ExoPlaybackError = (function (_super) {
    __extends(ExoPlaybackError, _super);
    function ExoPlaybackError(errorType, errorMessage, nativeException) {
        var _this = _super.call(this, "ExoPlaybackError<" + errorType + ">: " + errorMessage) || this;
        _this.errorType = errorType;
        _this.errorMessage = errorMessage;
        _this.nativeException = nativeException;
        Object.setPrototypeOf(_this, ExoPlaybackError.prototype);
        return _this;
    }
    return ExoPlaybackError;
}(Error));
exports.ExoPlaybackError = ExoPlaybackError;
function ensureNativeClasses() {
    if (TNSPlayerEvent) {
        return;
    }
    var TNSPlayerMetadataImpl = (function (_super) {
        __extends(TNSPlayerMetadataImpl, _super);
        function TNSPlayerMetadataImpl(owner) {
            var _this = _super.call(this) || this;
            _this.cls = "TNSPlayerMetadataImpl<" + ++TNSPlayerMetadataImpl_1.instanceNo + ">";
            _this._owner = new WeakRef(owner);
            return global.__native(_this);
        }
        TNSPlayerMetadataImpl_1 = TNSPlayerMetadataImpl;
        Object.defineProperty(TNSPlayerMetadataImpl.prototype, "owner", {
            get: function () {
                var _a;
                return (_a = this._owner) === null || _a === void 0 ? void 0 : _a.get();
            },
            enumerable: true,
            configurable: true
        });
        TNSPlayerMetadataImpl.prototype.onMetadata = function (metadata) {
            var _a, _b;
            var length = metadata.length();
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    var entry = metadata.get(i);
                    if (entry instanceof com.google.android.exoplayer2.metadata.icy.IcyInfo) {
                        var icyInfo = entry;
                        (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onMetadataReceived(icyInfo.title);
                    }
                    else if (entry instanceof com.google.android.exoplayer2.metadata.icy.IcyHeaders) {
                        var icyHeaders = entry;
                        (_b = this.owner) === null || _b === void 0 ? void 0 : _b._onMetadataReceived('', icyHeaders.name, icyHeaders.genre);
                    }
                }
            }
        };
        var TNSPlayerMetadataImpl_1;
        TNSPlayerMetadataImpl.instanceNo = 0;
        TNSPlayerMetadataImpl = TNSPlayerMetadataImpl_1 = __decorate([
            Interfaces([com.google.android.exoplayer2.metadata.MetadataOutput]),
            __metadata("design:paramtypes", [dk.nota.MediaService])
        ], TNSPlayerMetadataImpl);
        return TNSPlayerMetadataImpl;
    }(java.lang.Object));
    TNSPlayerMetadata = TNSPlayerMetadataImpl;
    var TNSPlayerEventImpl = (function (_super) {
        __extends(TNSPlayerEventImpl, _super);
        function TNSPlayerEventImpl(owner) {
            var _this = _super.call(this) || this;
            _this.cls = "TNSPlayerEventImpl<" + ++TNSPlayerEventImpl_1.instanceNo + ">";
            _this._owner = new WeakRef(owner);
            return global.__native(_this);
        }
        TNSPlayerEventImpl_1 = TNSPlayerEventImpl;
        Object.defineProperty(TNSPlayerEventImpl.prototype, "owner", {
            get: function () {
                var _a;
                return (_a = this._owner) === null || _a === void 0 ? void 0 : _a.get();
            },
            enumerable: true,
            configurable: true
        });
        TNSPlayerEventImpl.prototype.onIsPlayingChanged = function (isPlaying) {
            var _a, _b, _c, _d;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onIsPlayingChanged(" + isPlaying + ")", audioplayer_types_1.notaAudioCategory);
            }
            if (isPlaying) {
                (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onPlaying();
            }
            else if (((_c = (_b = this.owner) === null || _b === void 0 ? void 0 : _b.exoPlayer) === null || _c === void 0 ? void 0 : _c.getPlaybackState()) === com.google.android.exoplayer2.Player.STATE_READY) {
                (_d = this.owner) === null || _d === void 0 ? void 0 : _d._onPaused();
            }
        };
        TNSPlayerEventImpl.prototype.onTimelineChanged = function (timeline, reason) {
            var _a, _b;
            var manifest = (_b = (_a = this.owner) === null || _a === void 0 ? void 0 : _a.exoPlayer) === null || _b === void 0 ? void 0 : _b.getCurrentManifest();
            switch (reason) {
                case com.google.android.exoplayer2.Player.TIMELINE_CHANGE_REASON_PREPARED: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onTimelineChanged() - reason = \"prepared\" manifest:" + manifest, audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.TIMELINE_CHANGE_REASON_RESET: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onTimelineChanged() - reason = \"reset\" manifest:" + manifest, audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.TIMELINE_CHANGE_REASON_DYNAMIC: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onTimelineChanged() - reason = \"dynamic\" manifest:" + manifest, audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                default: {
                    trace.write(this.cls + ".onTimelineChanged() - reason = \"dynamic\" reason:\"" + reason + "\" manifest:" + manifest, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    break;
                }
            }
        };
        TNSPlayerEventImpl.prototype.onTracksChanged = function (trackGroups, trackSelections) {
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onTracksChanged(" + trackGroups + ", " + trackSelections + ")", audioplayer_types_1.notaAudioCategory);
            }
        };
        TNSPlayerEventImpl.prototype.onLoadingChanged = function (isLoading) {
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onLoadingChanged(" + isLoading + ")", audioplayer_types_1.notaAudioCategory);
            }
        };
        TNSPlayerEventImpl.prototype.onPlayerStateChanged = function (playWhenReady, playbackState) {
            var _a, _b, _c, _d, _e, _f, _g;
            switch (playbackState) {
                case com.google.android.exoplayer2.Player.STATE_BUFFERING: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPlayerStateChanged(" + playWhenReady + ", " + playbackState + "). State = 'buffering'", audioplayer_types_1.notaAudioCategory);
                    }
                    (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onBuffering();
                    if (!playWhenReady) {
                        (_b = this.owner) === null || _b === void 0 ? void 0 : _b._onPaused();
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.STATE_IDLE: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPlayerStateChanged(" + playWhenReady + ", " + playbackState + "). State = 'idle'", audioplayer_types_1.notaAudioCategory);
                    }
                    (_c = this.owner) === null || _c === void 0 ? void 0 : _c._onStopped();
                    break;
                }
                case com.google.android.exoplayer2.Player.STATE_ENDED: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPlayerStateChanged(" + playWhenReady + ", " + playbackState + "). State = 'ended'", audioplayer_types_1.notaAudioCategory);
                    }
                    (_d = this.owner) === null || _d === void 0 ? void 0 : _d._onEndOfTrackReached();
                    if (!((_f = (_e = this.owner) === null || _e === void 0 ? void 0 : _e.exoPlayer) === null || _f === void 0 ? void 0 : _f.hasNext())) {
                        (_g = this.owner) === null || _g === void 0 ? void 0 : _g._onEndOfPlaylistReached();
                    }
                    return;
                }
                case com.google.android.exoplayer2.Player.STATE_READY: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPlayerStateChanged(" + playWhenReady + ", " + playbackState + "). State = 'ready'", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                default: {
                    trace.write(this.cls + ".onPlayerStateChanged(" + playWhenReady + ", " + playbackState + "). State is unknown", audioplayer_types_1.notaAudioCategory);
                    break;
                }
            }
        };
        TNSPlayerEventImpl.prototype.onRepeatModeChanged = function (repeatMode) {
            switch (repeatMode) {
                case com.google.android.exoplayer2.Player.REPEAT_MODE_ALL: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onRepeatModeChanged() - " + repeatMode + " === 'ALL'", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.REPEAT_MODE_OFF: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onRepeatModeChanged() - " + repeatMode + " === 'OFF'", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.REPEAT_MODE_ONE: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onRepeatModeChanged() - " + repeatMode + " === 'ONE'", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                default: {
                    trace.write(this.cls + ".onRepeatModeChanged() - " + repeatMode + " is unknown", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    break;
                }
            }
        };
        TNSPlayerEventImpl.prototype.onShuffleModeEnabledChanged = function (shuffleModeEnabled) {
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onShuffleModeEnabledChanged() - " + shuffleModeEnabled, audioplayer_types_1.notaAudioCategory);
            }
        };
        TNSPlayerEventImpl.prototype.onPlayerError = function (exoPlaybackException) {
            var _a;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onPlayerError(...)", audioplayer_types_1.notaAudioCategory);
            }
            var errorType = 'UNKNOWN';
            var errorMessage = '';
            switch (exoPlaybackException.type) {
                case com.google.android.exoplayer2.ExoPlaybackException.TYPE_UNEXPECTED: {
                    errorType = 'UNEXPECTED';
                    errorMessage = exoPlaybackException.getUnexpectedException().getMessage();
                    break;
                }
                case com.google.android.exoplayer2.ExoPlaybackException.TYPE_SOURCE: {
                    errorType = 'SOURCE';
                    errorMessage = exoPlaybackException.getSourceException().getMessage();
                    break;
                }
                case com.google.android.exoplayer2.ExoPlaybackException.TYPE_RENDERER: {
                    errorType = 'RENDERER';
                    errorMessage = exoPlaybackException.getRendererException().getMessage();
                    break;
                }
                case com.google.android.exoplayer2.ExoPlaybackException.TYPE_REMOTE: {
                    errorType = 'REMOTE';
                    break;
                }
                case com.google.android.exoplayer2.ExoPlaybackException.TYPE_OUT_OF_MEMORY: {
                    errorType = 'TYPE_OUT_OF_MEMORY';
                    errorMessage = exoPlaybackException.getOutOfMemoryError().getMessage();
                    break;
                }
            }
            var error = new ExoPlaybackError(errorType, errorMessage, exoPlaybackException);
            trace.write(this.cls + ".onPlayerError() - " + error.message, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
            (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onPlaybackError(error);
        };
        TNSPlayerEventImpl.prototype.onPositionDiscontinuity = function (reason) {
            var _a;
            switch (reason) {
                case com.google.android.exoplayer2.Player.DISCONTINUITY_REASON_AD_INSERTION: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPositionDiscontinuity() - reason = \"DISCONTINUITY_REASON_AD_INSERTION\"", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.DISCONTINUITY_REASON_INTERNAL: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPositionDiscontinuity() - reason = \"DISCONTINUITY_REASON_INTERNAL\"", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.DISCONTINUITY_REASON_PERIOD_TRANSITION: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPositionDiscontinuity() - reason = \"DISCONTINUITY_REASON_PERIOD_TRANSITION\"", audioplayer_types_1.notaAudioCategory);
                    }
                    (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onEndOfTrackReached();
                    break;
                }
                case com.google.android.exoplayer2.Player.DISCONTINUITY_REASON_SEEK: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPositionDiscontinuity() - reason = \"DISCONTINUITY_REASON_SEEK\"", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                case com.google.android.exoplayer2.Player.DISCONTINUITY_REASON_SEEK_ADJUSTMENT: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPositionDiscontinuity() - reason = \"DISCONTINUITY_REASON_SEEK_ADJUSTMENT\"", audioplayer_types_1.notaAudioCategory);
                    }
                    break;
                }
                default: {
                    trace.write(this.cls + ".onPositionDiscontinuity() - reason = \"" + reason + "\" is unknown", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    break;
                }
            }
        };
        TNSPlayerEventImpl.prototype.onPlaybackParametersChanged = function (playbackParameters) {
            var pitch = playbackParameters.pitch, speed = playbackParameters.speed, skipSilence = playbackParameters.skipSilence;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onPlaybackParametersChanged() - " + JSON.stringify({ pitch: pitch, speed: speed, skipSilence: skipSilence }), audioplayer_types_1.notaAudioCategory);
            }
        };
        TNSPlayerEventImpl.prototype.onSeekProcessed = function () {
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onSeekProcessed()", audioplayer_types_1.notaAudioCategory);
            }
        };
        TNSPlayerEventImpl.prototype.onPlaybackSuppressionReasonChanged = function (reason) {
            var _a, _b, _c;
            switch (reason) {
                case com.google.android.exoplayer2.Player.PLAYBACK_SUPPRESSION_REASON_NONE: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPlaybackSuppressionReasonChanged() - reason = none", audioplayer_types_1.notaAudioCategory);
                    }
                    (_a = this.owner) === null || _a === void 0 ? void 0 : _a._onPlaybackSuspend(audioplayer_types_1.PlaybackSuspend.None);
                    break;
                }
                case com.google.android.exoplayer2.Player.PLAYBACK_SUPPRESSION_REASON_TRANSIENT_AUDIO_FOCUS_LOSS: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPlaybackSuppressionReasonChanged() - reason = transient audio focus loss", audioplayer_types_1.notaAudioCategory);
                    }
                    (_b = this.owner) === null || _b === void 0 ? void 0 : _b._onPlaybackSuspend(audioplayer_types_1.PlaybackSuspend.FocusLoss);
                    break;
                }
                default: {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".onPlaybackSuppressionReasonChanged() - unknown reason", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    }
                    (_c = this.owner) === null || _c === void 0 ? void 0 : _c._onPlaybackSuspend(audioplayer_types_1.PlaybackSuspend.Unknown);
                }
            }
        };
        var TNSPlayerEventImpl_1;
        TNSPlayerEventImpl.instanceNo = 0;
        TNSPlayerEventImpl = TNSPlayerEventImpl_1 = __decorate([
            Interfaces([com.google.android.exoplayer2.Player.EventListener]),
            __metadata("design:paramtypes", [dk.nota.MediaService])
        ], TNSPlayerEventImpl);
        return TNSPlayerEventImpl;
    }(java.lang.Object));
    TNSPlayerEvent = TNSPlayerEventImpl;
    var TNSMediaDescriptionAdapterImpl = (function (_super) {
        __extends(TNSMediaDescriptionAdapterImpl, _super);
        function TNSMediaDescriptionAdapterImpl(owner) {
            var _this = _super.call(this) || this;
            _this.cls = "TNSMediaDescriptionAdapterImpl<" + ++TNSMediaDescriptionAdapterImpl_1.instanceNo + ">";
            _this._owner = new WeakRef(owner);
            return global.__native(_this);
        }
        TNSMediaDescriptionAdapterImpl_1 = TNSMediaDescriptionAdapterImpl;
        Object.defineProperty(TNSMediaDescriptionAdapterImpl.prototype, "owner", {
            get: function () {
                var _a;
                return (_a = this._owner) === null || _a === void 0 ? void 0 : _a.get();
            },
            enumerable: true,
            configurable: true
        });
        TNSMediaDescriptionAdapterImpl.prototype.createCurrentContentIntent = function (player) {
            var _a, _b;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".createCurrentContentIntent(" + player + ")", audioplayer_types_1.notaAudioCategory);
            }
            return android.app.PendingIntent.getActivity(this.owner, 0, new android.content.Intent(this.owner, (_b = (_a = nsApp.android.startActivity) === null || _a === void 0 ? void 0 : _a.getClass(), (_b !== null && _b !== void 0 ? _b : dk.nota.MediaService.class))), android.app.PendingIntent.FLAG_UPDATE_CURRENT);
        };
        TNSMediaDescriptionAdapterImpl.prototype.getCurrentContentText = function (player) {
            var _a, _b;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".getCurrentContentText(" + player + ")", audioplayer_types_1.notaAudioCategory);
            }
            return _b = (_a = this._getTrackInfo(player)) === null || _a === void 0 ? void 0 : _a.album, (_b !== null && _b !== void 0 ? _b : null);
        };
        TNSMediaDescriptionAdapterImpl.prototype.getCurrentContentTitle = function (player) {
            var _a, _b;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".getCurrentContentTitle(" + player + ")", audioplayer_types_1.notaAudioCategory);
            }
            return _b = (_a = this._getTrackInfo(player)) === null || _a === void 0 ? void 0 : _a.title, (_b !== null && _b !== void 0 ? _b : null);
        };
        TNSMediaDescriptionAdapterImpl.prototype.getCurrentLargeIcon = function (player, callback) {
            var _a, _b, _c, _d;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".getCurrentLargeIcon(" + player + ")", audioplayer_types_1.notaAudioCategory);
            }
            var track = this._getTrackInfo(player);
            if (!((_a = track) === null || _a === void 0 ? void 0 : _a.albumArtUrl)) {
                return null;
            }
            var lastLoadedAlbumArt = (_b = this.owner) === null || _b === void 0 ? void 0 : _b._lastLoadedAlbumArt;
            if (((_c = lastLoadedAlbumArt) === null || _c === void 0 ? void 0 : _c.url) === track.albumArtUrl) {
                trace.write(this.cls + ".getCurrentLargeIcon(" + player + ") - using lastLoadedAlbumArt", audioplayer_types_1.notaAudioCategory);
                return lastLoadedAlbumArt.bitmap;
            }
            (_d = this.owner) === null || _d === void 0 ? void 0 : _d._loadAlbumArt(track, callback);
            return null;
        };
        TNSMediaDescriptionAdapterImpl.prototype.getCurrentSubText = function (player) {
            var _a, _b;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".getCurrentSubText(" + player + ")", audioplayer_types_1.notaAudioCategory);
            }
            return _b = (_a = this._getTrackInfo(player)) === null || _a === void 0 ? void 0 : _a.artist, (_b !== null && _b !== void 0 ? _b : null);
        };
        TNSMediaDescriptionAdapterImpl.prototype._getTrackInfo = function (player) {
            var _a;
            var index = player.getCurrentWindowIndex();
            return (_a = this.owner) === null || _a === void 0 ? void 0 : _a._getTrackInfo(index);
        };
        var TNSMediaDescriptionAdapterImpl_1;
        TNSMediaDescriptionAdapterImpl.instanceNo = 0;
        TNSMediaDescriptionAdapterImpl = TNSMediaDescriptionAdapterImpl_1 = __decorate([
            Interfaces([com.google.android.exoplayer2.ui.PlayerNotificationManager.MediaDescriptionAdapter]),
            __metadata("design:paramtypes", [dk.nota.MediaService])
        ], TNSMediaDescriptionAdapterImpl);
        return TNSMediaDescriptionAdapterImpl;
    }(java.lang.Object));
    TNSMediaDescriptionAdapter = TNSMediaDescriptionAdapterImpl;
    var TNSNotificationListenerImpl = (function (_super) {
        __extends(TNSNotificationListenerImpl, _super);
        function TNSNotificationListenerImpl(owner) {
            var _this = _super.call(this) || this;
            _this.cls = "TNSNotificationListenerImpl<" + ++TNSNotificationListenerImpl_1.instanceNo + ">";
            _this._owner = new WeakRef(owner);
            return global.__native(_this);
        }
        TNSNotificationListenerImpl_1 = TNSNotificationListenerImpl;
        Object.defineProperty(TNSNotificationListenerImpl.prototype, "owner", {
            get: function () {
                var _a;
                return (_a = this._owner) === null || _a === void 0 ? void 0 : _a.get();
            },
            enumerable: true,
            configurable: true
        });
        TNSNotificationListenerImpl.prototype.onNotificationPosted = function (notificationId, notification, ongoing) {
            var _a;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onNotificationPosted(" + notificationId + ", " + notification + ", " + ongoing + ")", audioplayer_types_1.notaAudioCategory);
            }
            (_a = this.owner) === null || _a === void 0 ? void 0 : _a._handleNotificationPosted(notificationId, notification);
        };
        TNSNotificationListenerImpl.prototype.onNotificationCancelled = function (notificationId, dismissedByUser) {
            if (dismissedByUser === void 0) { dismissedByUser = false; }
            var _a, _b;
            var cls = trace.isEnabled() && this.cls + ".onNotificationCancelled(id=" + notificationId + ", dismissedByUser=" + dismissedByUser + ") - " + this.owner;
            if (trace.isEnabled()) {
                trace.write("" + cls, audioplayer_types_1.notaAudioCategory);
            }
            if ((_a = this.owner) === null || _a === void 0 ? void 0 : _a._isForegroundService) {
                try {
                    this.owner.stopForeground(false);
                    this.owner._isForegroundService = false;
                }
                catch (err) {
                    trace.write(cls + " stopForeground failed! - " + err, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                }
            }
            (_b = this.owner) === null || _b === void 0 ? void 0 : _b.stopSelf();
        };
        TNSNotificationListenerImpl.prototype.onNotificationStarted = function (notificationId, notification) {
            var _a;
            if (trace.isEnabled()) {
                trace.write(this.cls + ".onNotificationStarted(" + notificationId + ", " + notification + ") is deprecated - why was this called?", audioplayer_types_1.notaAudioCategory, trace.messageType.warn);
            }
            (_a = this.owner) === null || _a === void 0 ? void 0 : _a._handleNotificationPosted(notificationId, notification);
        };
        var TNSNotificationListenerImpl_1;
        TNSNotificationListenerImpl.instanceNo = 0;
        TNSNotificationListenerImpl = TNSNotificationListenerImpl_1 = __decorate([
            Interfaces([com.google.android.exoplayer2.ui.PlayerNotificationManager.NotificationListener]),
            __metadata("design:paramtypes", [dk.nota.MediaService])
        ], TNSNotificationListenerImpl);
        return TNSNotificationListenerImpl;
    }(java.lang.Object));
    TNSNotificationListener = TNSNotificationListenerImpl;
}
//# sourceMappingURL=media-service.android.js.map