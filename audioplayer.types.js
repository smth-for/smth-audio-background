"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.notaAudioCategory = 'NotaAudioPlayer';
var MediaTrackData = (function () {
    function MediaTrackData(title, artist, album, albumArtUrl) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.albumArtUrl = albumArtUrl;
    }
    return MediaTrackData;
}());
exports.MediaTrackData = MediaTrackData;
var MediaTrack = (function (_super) {
    __extends(MediaTrack, _super);
    function MediaTrack(url, title, artist, album, albumArtUrl) {
        var _this = _super.call(this, title, artist, album, albumArtUrl) || this;
        _this.url = url;
        return _this;
    }
    return MediaTrack;
}(MediaTrackData));
exports.MediaTrack = MediaTrack;
var Playlist = (function () {
    function Playlist(UID) {
        var tracks = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            tracks[_i - 1] = arguments[_i];
        }
        this.UID = UID;
        this.UID = UID;
        this.tracks = tracks;
    }
    Object.defineProperty(Playlist.prototype, "length", {
        get: function () {
            return this.tracks.length;
        },
        enumerable: true,
        configurable: true
    });
    return Playlist;
}());
exports.Playlist = Playlist;
var PlaybackEvent;
(function (PlaybackEvent) {
    PlaybackEvent["Stopped"] = "Stopped";
    PlaybackEvent["Buffering"] = "Buffering";
    PlaybackEvent["Playing"] = "Playing";
    PlaybackEvent["Paused"] = "Paused";
    PlaybackEvent["EndOfTrackReached"] = "EndOfTrackReached";
    PlaybackEvent["EndOfPlaylistReached"] = "EndOfPlaylistReached";
    PlaybackEvent["EncounteredError"] = "EncounteredError";
    PlaybackEvent["TimeChanged"] = "TimeChanged";
    PlaybackEvent["SleepTimerChanged"] = "SleepTimerChanged";
    PlaybackEvent["SleepTimerEnded"] = "SleepTimerEnded";
    PlaybackEvent["WaitingForNetwork"] = "WaitingForNetwork";
    PlaybackEvent["PlaybackSuspend"] = "PlaybackSuspend";
    PlaybackEvent["ReceivedMetadata"] = "ReceivedMetadata";
})(PlaybackEvent = exports.PlaybackEvent || (exports.PlaybackEvent = {}));
var PlaybackSuspend;
(function (PlaybackSuspend) {
    PlaybackSuspend["None"] = "none";
    PlaybackSuspend["FocusLoss"] = "focus_lost";
    PlaybackSuspend["Unknown"] = "unknown";
})(PlaybackSuspend = exports.PlaybackSuspend || (exports.PlaybackSuspend = {}));
//# sourceMappingURL=audioplayer.types.js.map