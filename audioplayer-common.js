"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
var nsApp = require("@nativescript/core/application");
var observable_1 = require("@nativescript/core/data/observable");
var trace = require("@nativescript/core/trace");
var audioplayer_types_1 = require("./audioplayer.types");
__export(require("./audioplayer.types"));
var CommonAudioPlayer = (function (_super) {
    __extends(CommonAudioPlayer, _super);
    function CommonAudioPlayer() {
        var _this = _super.call(this) || this;
        _this.cls = "TNSAudioPlayer<" + ++CommonAudioPlayer.instanceNo + ">";
        _this._seekIntervalSeconds = 15;
        _this._playbackRate = 1;
        _this._sleepTimerPaused = false;
        _this._sleepTimerMillisecondsLeft = 0;
        nsApp.on(nsApp.exitEvent, _this._exitHandler, _this);
        return _this;
    }
    CommonAudioPlayer.prototype.preparePlaylist = function (playlist) {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.play = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.pause = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.stop = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.isPlaying = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.skipToPrevious = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.skipToNext = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.skipToPlaylistIndex = function (playlistIndex) {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.changeMediaTrackData = function (data) {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.setRate = function (rate) {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.getRate = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.getDuration = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.getCurrentTime = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.getCurrentPlaylistIndex = function () {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.seekTo = function (offset) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                throw new Error('Not implemented');
            });
        });
    };
    CommonAudioPlayer.prototype.setSeekIntervalSeconds = function (seconds) {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.prototype.getSeekIntervalSeconds = function () {
        return this._seekIntervalSeconds;
    };
    CommonAudioPlayer.prototype.setSleepTimer = function (milliseconds) {
        var _this = this;
        if (trace.isEnabled()) {
            trace.write(this.cls + ".setSleepTimer(" + milliseconds + ")", audioplayer_types_1.notaAudioCategory);
        }
        this.cancelSleepTimer();
        var countdownTick = 1000;
        this._sleepTimerMillisecondsLeft = milliseconds;
        this._sleepTimer = setInterval(function () {
            if (!_this._sleepTimerPaused && _this.isPlaying()) {
                _this._sleepTimerMillisecondsLeft = Math.max(_this._sleepTimerMillisecondsLeft - countdownTick, 0);
                _this._onSleepTimerChanged();
            }
            if (_this._sleepTimerMillisecondsLeft === 0) {
                if (_this.isPlaying()) {
                    _this._onSleepTimerExpired();
                }
                _this.cancelSleepTimer();
            }
        }, countdownTick);
        this._onSleepTimerChanged();
    };
    CommonAudioPlayer.prototype.getSleepTimerRemaining = function () {
        return this._sleepTimerMillisecondsLeft;
    };
    CommonAudioPlayer.prototype.cancelSleepTimer = function () {
        if (trace.isEnabled()) {
            trace.write(this.cls + ".cancelSleepTimer()", audioplayer_types_1.notaAudioCategory);
        }
        if (this._sleepTimer == null) {
            return;
        }
        clearInterval(this._sleepTimer);
        this._sleepTimer = null;
        this._sleepTimerMillisecondsLeft = 0;
        this._onSleepTimerChanged();
    };
    CommonAudioPlayer.prototype.pauseSleepTimer = function () {
        if (this._sleepTimer == null) {
            return;
        }
        if (trace.isEnabled()) {
            trace.write(this.cls + ".pauseSleepTimer()", audioplayer_types_1.notaAudioCategory);
        }
        this._sleepTimerPaused = true;
    };
    CommonAudioPlayer.prototype.resumeSleepTimer = function () {
        if (this._sleepTimer == null) {
            return;
        }
        if (trace.isEnabled()) {
            trace.write(this.cls + ".resumeSleepTimer()", audioplayer_types_1.notaAudioCategory);
        }
        this._sleepTimerPaused = false;
    };
    CommonAudioPlayer.prototype.loadPlaylist = function (playlist, startIndex, startOffset) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, this.preparePlaylist(playlist)];
                    case 1:
                        _a.sent();
                        if (!(startIndex != null && startOffset != null)) return [3, 3];
                        return [4, this.skipToPlaylistIndexAndOffset(startIndex, startOffset)];
                    case 2:
                        _a.sent();
                        return [2];
                    case 3:
                        if (!(startIndex != null)) return [3, 5];
                        return [4, this.skipToPlaylistIndex(startIndex)];
                    case 4:
                        _a.sent();
                        return [2];
                    case 5: return [2];
                }
            });
        });
    };
    CommonAudioPlayer.prototype.skipToPlaylistIndexAndOffset = function (playlistIndex, offset) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, this.getCurrentPlaylistIndex()];
                    case 1:
                        if (!((_a.sent()) === playlistIndex)) return [3, 3];
                        return [4, this.seekTo(offset)];
                    case 2:
                        _a.sent();
                        return [2];
                    case 3:
                        if (offset > 0) {
                            if (trace.isEnabled()) {
                                trace.write("Set queuedSeek to " + offset, audioplayer_types_1.notaAudioCategory);
                            }
                            this._queuedSeekTo = offset;
                        }
                        this.skipToPlaylistIndex(playlistIndex);
                        return [2];
                }
            });
        });
    };
    CommonAudioPlayer.prototype.seekRelative = function (relativeOffset) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c, _d, _e, _f, _g;
            return __generator(this, function (_h) {
                switch (_h.label) {
                    case 0:
                        _a = this.seekTo;
                        _c = (_b = Math).min;
                        return [4, this.getDuration()];
                    case 1:
                        _d = [_h.sent()];
                        _f = (_e = Math).max;
                        _g = [0];
                        return [4, this.getCurrentTime()];
                    case 2:
                        _a.apply(this, [_c.apply(_b, _d.concat([_f.apply(_e, _g.concat([(_h.sent()) + relativeOffset]))]))]);
                        return [2];
                }
            });
        });
    };
    CommonAudioPlayer.prototype.setPlaybackEventListener = function (listener) {
        trace.write(this.cls + ".setPlaybackEventListener(..) is deprecated", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
        this._listener = listener;
    };
    CommonAudioPlayer.prototype.getCurrentPlaylistUID = function () {
        var _a, _b;
        return _b = (_a = this.playlist) === null || _a === void 0 ? void 0 : _a.UID, (_b !== null && _b !== void 0 ? _b : null);
    };
    CommonAudioPlayer.prototype._onTimeChanged = function (currentTime, duration, playlistIndex) {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.timeChangedEvent,
            currentTime: currentTime,
            playlistIndex: playlistIndex,
            duration: duration,
        });
        var parsedDuration = typeof duration === 'number' ? duration : -1;
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.TimeChanged, { currentTime: currentTime, playlistIndex: playlistIndex, duration: parsedDuration });
    };
    CommonAudioPlayer.prototype._onMetadataReceived = function (title, name, genre) {
        if (name === void 0) { name = ''; }
        if (genre === void 0) { genre = ''; }
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.metadataReceivedEvent,
            data: {
                title: title,
                name: name,
                genre: genre,
            },
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.ReceivedMetadata, { title: title, name: name, genre: genre });
    };
    CommonAudioPlayer.prototype._onPlaying = function () {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var _b, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _b = this.notify;
                        _c = {
                            object: this,
                            eventName: CommonAudioPlayer.playingEvent
                        };
                        return [4, this.getCurrentTime()];
                    case 1:
                        _c.currentTime = _d.sent();
                        return [4, this.getCurrentPlaylistIndex()];
                    case 2:
                        _c.playlistIndex = _d.sent();
                        return [4, this.getDuration()];
                    case 3:
                        _b.apply(this, [(_c.duration = _d.sent(),
                                _c)]);
                        this.resumeSleepTimer();
                        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.Playing);
                        return [2];
                }
            });
        });
    };
    CommonAudioPlayer.prototype._onPaused = function () {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var _b, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        _b = this.notify;
                        _c = {
                            object: this,
                            eventName: CommonAudioPlayer.pausedEvent
                        };
                        return [4, this.getCurrentTime()];
                    case 1:
                        _c.currentTime = _d.sent();
                        return [4, this.getCurrentPlaylistIndex()];
                    case 2:
                        _c.playlistIndex = _d.sent();
                        return [4, this.getDuration()];
                    case 3:
                        _b.apply(this, [(_c.duration = _d.sent(),
                                _c)]);
                        this.pauseSleepTimer();
                        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.Paused);
                        return [2];
                }
            });
        });
    };
    CommonAudioPlayer.prototype._onStopped = function () {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.stoppedEvent,
        });
        this.cancelSleepTimer();
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.Stopped);
    };
    CommonAudioPlayer.prototype._onEndOfPlaylistReached = function () {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.endOfPlaylistReachedEvent,
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.EndOfPlaylistReached);
    };
    CommonAudioPlayer.prototype._onEndOfTrackReached = function (endedTrackIndex) {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.endOfTrackReachedEvent,
            endedTrackIndex: endedTrackIndex,
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.EndOfTrackReached);
    };
    CommonAudioPlayer.prototype._onSleepTimerExpired = function () {
        var _a;
        this.pause();
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.sleepTimerEndedEvent,
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.SleepTimerEnded);
    };
    CommonAudioPlayer.prototype._onSleepTimerChanged = function () {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.sleepTimerChangedEvent,
            remaining: this._sleepTimerMillisecondsLeft,
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.SleepTimerChanged);
    };
    CommonAudioPlayer.prototype._onBuffering = function () {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.bufferingEvent,
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.Buffering);
    };
    CommonAudioPlayer.prototype._onPlaybackError = function (errorData) {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.encounteredErrorEvent,
            error: errorData,
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.EncounteredError, errorData);
    };
    CommonAudioPlayer.prototype._onWaitingForNetwork = function () {
        var _a;
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.waitingForNetworkEvent,
        });
        (_a = this._listener) === null || _a === void 0 ? void 0 : _a.onPlaybackEvent(audioplayer_types_1.PlaybackEvent.WaitingForNetwork);
    };
    CommonAudioPlayer.prototype._onPlaybackSuspended = function (reason) {
        this.notify({
            object: this,
            eventName: CommonAudioPlayer.playbackSuspendEvent,
            reason: reason,
        });
    };
    CommonAudioPlayer.prototype.destroy = function () {
        nsApp.off(nsApp.exitEvent, this._exitHandler, this);
    };
    CommonAudioPlayer.prototype._exitHandler = function (args) {
        throw new Error('Not implemented');
    };
    CommonAudioPlayer.instanceNo = 0;
    CommonAudioPlayer.bufferingEvent = 'Buffering';
    CommonAudioPlayer.encounteredErrorEvent = 'EncounteredError';
    CommonAudioPlayer.endOfPlaylistReachedEvent = 'EndOfPlaylistReached';
    CommonAudioPlayer.endOfTrackReachedEvent = 'EndOfTrackReached';
    CommonAudioPlayer.pausedEvent = 'Paused';
    CommonAudioPlayer.playbackSuspendEvent = 'PlaybackSuspend';
    CommonAudioPlayer.playingEvent = 'Playing';
    CommonAudioPlayer.sleepTimerChangedEvent = 'SleepTimerChanged';
    CommonAudioPlayer.sleepTimerEndedEvent = 'SleepTimerEnded';
    CommonAudioPlayer.stoppedEvent = 'Stopped';
    CommonAudioPlayer.timeChangedEvent = 'TimeChanged';
    CommonAudioPlayer.metadataReceivedEvent = 'MetadataReceived';
    CommonAudioPlayer.waitingForNetworkEvent = 'WaitingForNetwork';
    return CommonAudioPlayer;
}(observable_1.Observable));
exports.CommonAudioPlayer = CommonAudioPlayer;
//# sourceMappingURL=audioplayer-common.js.map