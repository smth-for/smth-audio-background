/// <reference path="native-definitions/android.d.ts" />
import { TNSAudioPlayer } from './audioplayer';
import { MediaTrack, MediaTrackData, PlaybackSuspend, Playlist } from './audioplayer.types';
export declare namespace dk {
    namespace nota {
        class MediaService extends android.app.Service {
            private _cls;
            private get cls();
            constructor();
            exoPlayer?: com.google.android.exoplayer2.SimpleExoPlayer;
            private _mediaSession?;
            private get _sessionToken();
            private _playerNotificationManager?;
            private _mediaSessionConnector?;
            private _mediaSessionMetadataProvider?;
            private _playlist?;
            _isForegroundService: boolean;
            private _owner?;
            private get owner();
            private _rate;
            private _seekIntervalSeconds;
            private _intentReqCode;
            private _timeChangeInterval;
            private _albumArts?;
            private get albumArts();
            _lastLoadedAlbumArt?: {
                url: string;
                bitmap: android.graphics.Bitmap;
            };
            onCreate(): void;
            _getTrackInfo(index: number): MediaTrack | undefined;
            private lastPosition;
            private lastWindowIndex;
            private _handleTimeChange;
            _onPlaying(): void;
            _onPaused(): void;
            _onStopped(): void;
            _onEndOfPlaylistReached(): void;
            _onEndOfTrackReached(): void;
            _onBuffering(): void;
            _onMetadataReceived(title: string, name?: string, genre?: string): void;
            _onWaitingForNetwork(): void;
            _onPlaybackError(errorData: any): void;
            _onPlaybackSuspend(reason: PlaybackSuspend): void;
            onDestroy(): void;
            _handleNotificationPosted(notificationId: number, notification: android.app.Notification): void;
            onBind(intent: android.content.Intent): android.os.IBinder;
            onStartCommand(intent: android.content.Intent, flags: number, startId: number): number;
            setOwner(owner: TNSAudioPlayer): void;
            preparePlaylist(playlist: Playlist): Promise<void>;
            setSeekIntervalSeconds(seconds: number): void;
            setRate(rate: number): void;
            getRate(): number;
            isPlaying(): boolean;
            updateCurrentTrackData(data: MediaTrackData): void;
            play(): void;
            pause(): void;
            stop(): void;
            onTaskRemoved(rootIntent: any): void;
            private _makeAlbumArtImageSource;
            _loadAlbumArt(track: MediaTrack, callback: com.google.android.exoplayer2.ui.PlayerNotificationManager.BitmapCallback): Promise<void>;
            private _checkUrlAllowed;
        }
        class TNSMediaButtonReceiver extends androidx.media.session.MediaButtonReceiver {
            onReceive(context: android.content.Context, intent: android.content.Intent): void;
        }
        namespace MediaService {
            class LocalBinder extends android.os.Binder {
                private owner;
                constructor(owner: MediaService);
                getService(): MediaService;
            }
        }
    }
}
export declare class ExoPlaybackError extends Error {
    errorType: string;
    errorMessage: string;
    nativeException: com.google.android.exoplayer2.ExoPlaybackException;
    constructor(errorType: string, errorMessage: string, nativeException: com.google.android.exoplayer2.ExoPlaybackException);
}
