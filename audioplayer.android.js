"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var trace = require("@nativescript/core/trace");
var page_1 = require("@nativescript/core/ui/page/page");
var utils = require("@nativescript/core/utils/utils");
var audioplayer_common_1 = require("./audioplayer-common");
var audioplayer_types_1 = require("./audioplayer.types");
require("./media-service");
var TNSAudioPlayer = (function (_super) {
    __extends(TNSAudioPlayer, _super);
    function TNSAudioPlayer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cls = "TNSAudioPlayer.android<" + ++audioplayer_common_1.CommonAudioPlayer.instanceNo + ">";
        _this._serviceConnection = new android.content.ServiceConnection({
            onServiceConnected: function (componentName, binder) {
                if (trace.isEnabled()) {
                    trace.write(_this.cls + ".onServiceConnected(" + componentName.toString() + ", " + binder + ")", audioplayer_types_1.notaAudioCategory);
                }
                var mediaService = binder.getService();
                if (!mediaService) {
                    trace.write(_this.cls + ".onServiceConnected(" + componentName.toString() + ", " + binder + ") - couldn't get mediaservice", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    _this._mediaServiceReject(new Error('MediaService not created'));
                    return;
                }
                mediaService.setOwner(_this);
                _this._mediaService = mediaService;
                _this._mediaServiceResolve(mediaService);
            },
            onBindingDied: function (componentName) {
                if (trace.isEnabled()) {
                    trace.write(_this.cls + ".onBindingDied(" + componentName + ")", audioplayer_types_1.notaAudioCategory);
                }
                _this._mediaServiceReject(new Error('Died'));
            },
            onNullBinding: function (componentName) {
                if (trace.isEnabled()) {
                    trace.write(_this.cls + ".onNullBinding(" + componentName + ")", audioplayer_types_1.notaAudioCategory);
                }
                _this._mediaServiceReject(new Error('Null binding'));
            },
            onServiceDisconnected: function (componentName) {
                if (trace.isEnabled()) {
                    trace.write(_this.cls + ".onServiceDisconnected(" + componentName + ")", audioplayer_types_1.notaAudioCategory);
                }
                _this._mediaServicePromise = undefined;
                _this._mediaService = undefined;
            },
        });
        return _this;
    }
    Object.defineProperty(TNSAudioPlayer.prototype, "context", {
        get: function () {
            return utils.ad.getApplicationContext();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TNSAudioPlayer.prototype, "mediaService", {
        get: function () {
            var _this = this;
            if (this._mediaService) {
                return Promise.resolve(this._mediaService);
            }
            if (!this._mediaServicePromise) {
                this._mediaServicePromise = new Promise(function (resolve, reject) {
                    _this._mediaServiceResolve = resolve;
                    _this._mediaServiceReject = reject;
                    _this.startMediaService();
                });
                var noop_1 = function () { };
                this._mediaServicePromise
                    .then(function (mediaService) {
                    _this._mediaServicePromise = undefined;
                    var seekIntervalSeconds = _this._seekIntervalSeconds;
                    if (typeof seekIntervalSeconds === 'number' && !Number.isNaN(seekIntervalSeconds)) {
                        mediaService.setSeekIntervalSeconds(seekIntervalSeconds);
                    }
                    var rate = _this._playbackRate;
                    if (typeof rate === 'number' && !Number.isNaN(rate)) {
                        mediaService.setRate(rate);
                    }
                }, function (err) {
                    page_1.traceWrite(_this.cls + " - couldn't init mediaService: " + (err.stack || err), audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                })
                    .then(function () {
                    _this._mediaServiceResolve = noop_1;
                    _this._mediaServiceReject = noop_1;
                });
            }
            return this._mediaServicePromise;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TNSAudioPlayer.prototype, "android", {
        get: function () {
            return this._mediaService;
        },
        enumerable: true,
        configurable: true
    });
    TNSAudioPlayer.prototype.preparePlaylist = function (playlist) {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4, this.mediaService];
                    case 1:
                        mediaService = _a.sent();
                        return [4, mediaService.preparePlaylist(playlist)];
                    case 2:
                        _a.sent();
                        return [3, 4];
                    case 3:
                        err_1 = _a.sent();
                        trace.write(this.cls + ".preparePlaylist() - " + err_1, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.getCurrentPlaylistIndex = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".getCurrentPlaylistIndex() - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2, -1];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        return [2, mediaService.exoPlayer.getCurrentWindowIndex()];
                    case 3:
                        err_2 = _a.sent();
                        trace.write(this.cls + ".getCurrentPlaylistIndex() - " + err_2, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [2, -1];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.play = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4, this.mediaService];
                    case 1:
                        mediaService = _a.sent();
                        mediaService.play();
                        return [3, 3];
                    case 2:
                        err_3 = _a.sent();
                        trace.write(this.cls + ".play() - " + err_3, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 3];
                    case 3: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.pause = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            if (trace.isEnabled()) {
                                trace.write(this.cls + ".pause() - no media service - cannot pause", audioplayer_types_1.notaAudioCategory);
                            }
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        mediaService.pause();
                        return [3, 4];
                    case 3:
                        err_4 = _a.sent();
                        trace.write(this.cls + ".pause() - " + err_4, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.stop = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.playlist = undefined;
                if (!this._mediaService) {
                    if (trace.isEnabled()) {
                        trace.write(this.cls + ".stop() - no media service - nothing to stop", audioplayer_types_1.notaAudioCategory);
                    }
                    return [2];
                }
                try {
                    this._mediaService.stop();
                }
                catch (err) {
                    trace.write(this.cls + ".stop() - " + err, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.isPlaying = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            if (trace.isEnabled()) {
                                trace.write(this.cls + ".isPlaying() - no media service. - is not playing", audioplayer_types_1.notaAudioCategory);
                            }
                            return [2, false];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        return [2, mediaService.isPlaying()];
                    case 3:
                        err_5 = _a.sent();
                        trace.write(this.cls + ".isPlaying() - " + err_5, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [2, false];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.seekTo = function (offset) {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".seekTo(" + offset + ") - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        mediaService.exoPlayer.seekTo(offset);
                        return [3, 4];
                    case 3:
                        err_6 = _a.sent();
                        trace.write(this.cls + ".seekTo(" + offset + ") - " + err_6, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.changeMediaTrackData = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".changeMediaTrackData(" + data.title + ") - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        mediaService.updateCurrentTrackData(data);
                        return [3, 4];
                    case 3:
                        err_7 = _a.sent();
                        trace.write(this.cls + ".changeMediaTrackData(" + data.title + ") - " + err_7, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.skipToPlaylistIndexAndOffset = function (playlistIndex, offset) {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".skipToPlaylistIndexAndOffset(" + playlistIndex + ", " + offset + ") - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        mediaService.exoPlayer.seekTo(playlistIndex, offset);
                        return [3, 4];
                    case 3:
                        err_8 = _a.sent();
                        trace.write(this.cls + ".skipToPlaylistIndexAndOffset(" + playlistIndex + ", " + offset + ") - " + err_8, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.skipToPrevious = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".skipToPrevious() - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        if (mediaService.exoPlayer.hasPrevious()) {
                            mediaService.exoPlayer.previous();
                        }
                        return [3, 4];
                    case 3:
                        err_9 = _a.sent();
                        trace.write(this.cls + ".skipToPrevious() - " + err_9, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.skipToNext = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".skipToNext() - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        if (mediaService.exoPlayer.hasNext()) {
                            mediaService.exoPlayer.next();
                        }
                        return [3, 4];
                    case 3:
                        err_10 = _a.sent();
                        trace.write(this.cls + ".skipToNext() - " + err_10, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.skipToPlaylistIndex = function (playlistIndex) {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".skipToPlaylistIndex(" + playlistIndex + ") - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        mediaService.exoPlayer.seekTo(playlistIndex, 0);
                        return [3, 4];
                    case 3:
                        err_11 = _a.sent();
                        trace.write(this.cls + ".skipToPlaylistIndex(" + playlistIndex + ") - " + err_11, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.setRate = function (rate) {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (typeof rate === 'number' && !Number.isNaN(rate)) {
                            this._playbackRate = rate;
                        }
                        else {
                            rate = 1;
                            this._playbackRate = rate;
                        }
                        if (!this._mediaService) {
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        mediaService.setRate(rate);
                        return [3, 4];
                    case 3:
                        err_12 = _a.sent();
                        trace.write(this.cls + ".setRate(" + rate + ") - " + err_12, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.getRate = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".getRate() - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2, this._playbackRate || 1];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        return [2, mediaService.getRate()];
                    case 3:
                        err_13 = _a.sent();
                        trace.write(this.cls + ".getRate() - " + err_13, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [2, this._playbackRate || 1];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.getDuration = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".getDuration() - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2, 0];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        return [2, mediaService.exoPlayer.getDuration()];
                    case 3:
                        err_14 = _a.sent();
                        trace.write(this.cls + ".getDuration() - " + err_14, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [2, 0];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.getCurrentTime = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._mediaService) {
                            trace.write(this.cls + ".getCurrentTime() - no media service.", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                            return [2, -1];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        return [2, mediaService.exoPlayer.getCurrentPosition()];
                    case 3:
                        err_15 = _a.sent();
                        trace.write(this.cls + ".getCurrentTime() - " + err_15, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [2, -1];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.setSeekIntervalSeconds = function (seconds) {
        return __awaiter(this, void 0, void 0, function () {
            var mediaService, err_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (trace.isEnabled()) {
                            trace.write(this.cls + ".setSeekIntervalSeconds(" + seconds + ")", audioplayer_types_1.notaAudioCategory);
                        }
                        this._seekIntervalSeconds = seconds;
                        if (!this._mediaService) {
                            return [2];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.mediaService];
                    case 2:
                        mediaService = _a.sent();
                        mediaService.setSeekIntervalSeconds(seconds);
                        return [3, 4];
                    case 3:
                        err_16 = _a.sent();
                        trace.write(this.cls + ".setSeekIntervalSeconds(" + seconds + ") - " + err_16, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 4];
                    case 4: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype.destroy = function () {
        if (trace.isEnabled()) {
            trace.write(this.cls + ".destroy()", audioplayer_types_1.notaAudioCategory);
        }
        this.stopMediaService();
        _super.prototype.destroy.call(this);
    };
    TNSAudioPlayer.prototype.startMediaService = function () {
        if (trace.isEnabled()) {
            trace.write(this.cls + ".startMediaService()", audioplayer_types_1.notaAudioCategory);
        }
        if (!this.context) {
            trace.write(this.cls + ".startMediaService() - no context, cannot start MediaService", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
            return;
        }
        var context = this.context;
        var foregroundNotificationIntent = new android.content.Intent(context, dk.nota.MediaService.class);
        context.startService(foregroundNotificationIntent);
        context.bindService(foregroundNotificationIntent, this._serviceConnection, android.content.Context.BIND_AUTO_CREATE);
    };
    TNSAudioPlayer.prototype.stopMediaService = function () {
        if (trace.isEnabled()) {
            trace.write(this.cls + ".stopMediaService()", audioplayer_types_1.notaAudioCategory);
        }
        this._mediaServicePromise = undefined;
        if (!this._mediaService) {
            if (trace.isEnabled()) {
                trace.write(this.cls + ".stopMediaService() - no media service", audioplayer_types_1.notaAudioCategory);
            }
            return;
        }
        var context = this.context;
        var foregroundNotificationIntent = new android.content.Intent(context, dk.nota.MediaService.class);
        context.unbindService(this._serviceConnection);
        context.stopService(foregroundNotificationIntent);
        this._mediaService.setOwner(null);
        this._mediaService.stopForeground(true);
        this._mediaService.stopSelf();
        delete this._mediaService;
    };
    TNSAudioPlayer.prototype._exitHandler = function (args) {
        var _a;
        var activity = args.android;
        if ((_a = activity) === null || _a === void 0 ? void 0 : _a.isFinishing()) {
            this.destroy();
            return;
        }
    };
    return TNSAudioPlayer;
}(audioplayer_common_1.CommonAudioPlayer));
exports.TNSAudioPlayer = TNSAudioPlayer;
//# sourceMappingURL=audioplayer.android.js.map