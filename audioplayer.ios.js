"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var image_source_1 = require("@nativescript/core/image-source");
var trace = require("@nativescript/core/trace");
var utils = require("@nativescript/core/utils/utils");
var audioplayer_common_1 = require("./audioplayer-common");
var audioplayer_types_1 = require("./audioplayer.types");
var AudioPlayerDelegateImpl = (function (_super) {
    __extends(AudioPlayerDelegateImpl, _super);
    function AudioPlayerDelegateImpl() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AudioPlayerDelegateImpl.new = function () {
        return _super.new.call(this);
    };
    AudioPlayerDelegateImpl.prototype.audioPlayerDidChangeStateFromTo = function (audioPlayer, from, state) {
        if (this.onStateChanged) {
            this.onStateChanged(from, state);
        }
    };
    AudioPlayerDelegateImpl.prototype.audioPlayerDidFindDurationFor = function (audioPlayer, duration, item) {
        if (this.onFoundDuration) {
            this.onFoundDuration(item, duration);
        }
    };
    AudioPlayerDelegateImpl.prototype.audioPlayerDidLoadEarliestLatestFor = function (audioPlayer, earliest, latest, item) {
        if (this.onBufferingUpdate) {
            this.onBufferingUpdate(item, earliest, latest);
        }
    };
    AudioPlayerDelegateImpl.prototype.audioPlayerDidUpdateEmptyMetadataOnWithData = function (audioPlayer, item, data) {
        if (this.onMetadataReceived) {
            this.onMetadataReceived(item, data);
        }
    };
    AudioPlayerDelegateImpl.prototype.audioPlayerDidUpdateProgressionToPercentageRead = function (audioPlayer, time, percentageRead) {
        if (this.onTimeUpdate) {
            this.onTimeUpdate(time);
        }
    };
    AudioPlayerDelegateImpl.prototype.audioPlayerWillStartPlaying = function (audioPlayer, item) {
        if (this.onWillStartPlayingItem) {
            this.onWillStartPlayingItem(item);
        }
    };
    AudioPlayerDelegateImpl.prototype.audioPlayerFinishedPlaying = function (audioPlayer, item) {
        if (this.onFinishedPlayingItem) {
            this.onFinishedPlayingItem(item);
        }
    };
    AudioPlayerDelegateImpl.ObjCProtocols = [AudioPlayerDelegate];
    return AudioPlayerDelegateImpl;
}(NSObject));
var TNSAudioPlayer = (function (_super) {
    __extends(TNSAudioPlayer, _super);
    function TNSAudioPlayer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cls = "TNSAudioPlayer.ios<" + ++audioplayer_common_1.CommonAudioPlayer.instanceNo + ">";
        _this._isSeeking = false;
        _this._isRetrievingArtwork = false;
        return _this;
    }
    TNSAudioPlayer.prototype.preparePlaylist = function (playlist) {
        return __awaiter(this, void 0, void 0, function () {
            var audioItems, _a, _b, track, audioItem;
            var e_1, _c;
            return __generator(this, function (_d) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".preparePlaylist($)", audioplayer_types_1.notaAudioCategory);
                }
                this.playlist = playlist;
                if (!this._player) {
                    this.setupAudioPlayer();
                }
                else {
                    this.stop();
                }
                audioItems = NSMutableArray.alloc().init();
                try {
                    for (_a = __values(playlist.tracks), _b = _a.next(); !_b.done; _b = _a.next()) {
                        track = _b.value;
                        audioItem = this.makeAudioItemForMediaTrack(track);
                        if (audioItem != null) {
                            audioItems.addObject(audioItem);
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                this._iosPlaylist = audioItems;
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.setupAudioPlayer = function () {
        if (trace.isEnabled()) {
            trace.write(this.cls + ".setupAudioPlayer()", audioplayer_types_1.notaAudioCategory);
        }
        this.setupDelegate();
        this._player = AudioPlayer.new();
        this._player.delegate = this._delegate;
        this._player.bufferingStrategy = 1;
        this._player.preferredBufferDurationBeforePlayback = 5;
        this._player.timePitchAlgorithm = AVAudioTimePitchAlgorithmTimeDomain;
        this._player.sessionCategory = AVAudioSessionCategoryPlayback;
        if (AVAudioSessionModeSpokenAudio) {
            trace.write(this.cls + ".setupAudioPlayer() - AVAudioSessionMode = " + AVAudioSessionModeSpokenAudio, audioplayer_types_1.notaAudioCategory);
            this._player.sessionMode = AVAudioSessionModeSpokenAudio;
        }
        this._player.allowExternalPlayback = true;
        this._player.remoteControlSkipIntervals = NSArrayFromItems([this._seekIntervalSeconds]);
        this._player.remoteCommandsEnabled = NSArrayFromItems([
            NSNumber.numberWithInt(9),
            NSNumber.numberWithInt(8),
            NSNumber.numberWithInt(5),
            NSNumber.numberWithInt(0),
            NSNumber.numberWithInt(4),
        ]);
        this.ios = this._player;
        trace.write(this.cls + ".setupAudioPlayer() - Player: " + this._player, audioplayer_types_1.notaAudioCategory);
        trace.write(this.cls + ".setupAudioPlayer() - Delegate: " + this._delegate, audioplayer_types_1.notaAudioCategory);
        trace.write(this.cls + ".setupAudioPlayer() - TimePitch Algorithm: " + this._player.timePitchAlgorithm, audioplayer_types_1.notaAudioCategory);
    };
    TNSAudioPlayer.prototype.play = function () {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_c) {
                try {
                    if (((_a = this._player) === null || _a === void 0 ? void 0 : _a.state) === 2) {
                        if (trace.isEnabled()) {
                            trace.write(this.cls + ".play() - resume", audioplayer_types_1.notaAudioCategory);
                        }
                        this._player.resume();
                    }
                    else if (((_b = this._player) === null || _b === void 0 ? void 0 : _b.state) === 3) {
                        this._player.playWithItemsStartAtIndex(this._iosPlaylist, 0);
                        if (trace.isEnabled()) {
                            trace.write(this.cls + ".play() - from start", audioplayer_types_1.notaAudioCategory);
                        }
                    }
                    else {
                        trace.write(this.cls + ".play() - unknown start state?", audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                    }
                }
                catch (err) {
                    trace.write(this.cls + ".play() - error: " + err, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.pause = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".pause()", audioplayer_types_1.notaAudioCategory);
                }
                if (this._player) {
                    this._player.pause();
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.stop = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".stop()", audioplayer_types_1.notaAudioCategory);
                }
                if (this._player) {
                    this._player.stop();
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.isPlaying = function () {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                return [2, ((_a = this._player) === null || _a === void 0 ? void 0 : _a.state) === 1];
            });
        });
    };
    TNSAudioPlayer.prototype.skipToNext = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this._player) {
                    this._player.nextOrStop();
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.skipToPrevious = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this._player) {
                    this._player.previous();
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.skipToPlaylistIndex = function (playlistIndex) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this._player) {
                    this._player.playWithItemsStartAtIndex(this._iosPlaylist, playlistIndex);
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.setRate = function (rate) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this._player) {
                    this._player.rate = rate;
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.getRate = function () {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".getRate(...) => " + ((_a = this._player) === null || _a === void 0 ? void 0 : _a.timePitchAlgorithm), audioplayer_types_1.notaAudioCategory);
                }
                return [2, this._player ? this._player.rate : 0];
            });
        });
    };
    TNSAudioPlayer.prototype._getDuration = function () {
        var _a, _b;
        if (((_a = this._player) === null || _a === void 0 ? void 0 : _a.currentItem) && ((_b = this._player) === null || _b === void 0 ? void 0 : _b.currentItemDuration)) {
            return Math.floor(this._player.currentItemDuration * 1000);
        }
        return -1;
    };
    TNSAudioPlayer.prototype.getDuration = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, this._getDuration()];
            });
        });
    };
    TNSAudioPlayer.prototype.getCurrentTime = function () {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_c) {
                if (((_a = this._player) === null || _a === void 0 ? void 0 : _a.currentItem) && ((_b = this._player) === null || _b === void 0 ? void 0 : _b.currentItemProgression)) {
                    return [2, Math.max(0, Math.floor(this._player.currentItemProgression * 1000))];
                }
                return [2, 0];
            });
        });
    };
    TNSAudioPlayer.prototype.getIndexForItem = function (item) {
        var result = this._iosPlaylist.indexOfObject(this._player.currentItem);
        if (result !== NSNotFound) {
            return result;
        }
        else {
            return -1;
        }
    };
    TNSAudioPlayer.prototype._getCurrentPlaylistIndex = function () {
        var _a;
        if (this._iosPlaylist && ((_a = this._player) === null || _a === void 0 ? void 0 : _a.currentItem)) {
            return this.getIndexForItem(this._player.currentItem);
        }
        return null;
    };
    TNSAudioPlayer.prototype.getCurrentPlaylistIndex = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2, this._getCurrentPlaylistIndex()];
            });
        });
    };
    TNSAudioPlayer.prototype.changeMediaTrackData = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this._player) {
                    this._iosChangeTrackData(data);
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.seekTo = function (milliseconds) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this._player) {
                    this._iosSeekTo(milliseconds);
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.setSeekIntervalSeconds = function (seconds) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (trace.isEnabled()) {
                    trace.write(this.cls + ".setSeekIntervalSeconds(" + seconds + ")", audioplayer_types_1.notaAudioCategory);
                }
                this._seekIntervalSeconds = seconds;
                if (this._player) {
                    this._player.remoteControlSkipIntervals = NSArray.arrayWithObject(seconds);
                }
                return [2];
            });
        });
    };
    TNSAudioPlayer.prototype.destroy = function () {
        if (trace.isEnabled()) {
            trace.write(this.cls + ".destroy()", audioplayer_types_1.notaAudioCategory);
        }
        this.stop();
        delete this._player;
        delete this._delegate;
        delete this._iosPlaylist;
        _super.prototype.destroy.call(this);
    };
    TNSAudioPlayer.prototype.setupDelegate = function () {
        var _this = this;
        this._delegate = AudioPlayerDelegateImpl.new();
        this._delegate.onTimeUpdate = function (seconds) {
            var timeMilliseconds = Math.floor(seconds * 1000);
            if (_this._isSeeking) {
                if (trace.isEnabled()) {
                    trace.write(_this.cls + " - time-update skipped, we're seeking", audioplayer_types_1.notaAudioCategory);
                }
            }
            else {
                _this._onTimeChanged(timeMilliseconds, _this._getDuration(), _this._getCurrentPlaylistIndex());
            }
        };
        this._delegate.onBufferingUpdate = function (item, earliest, latest) {
            if (trace.isEnabled()) {
                trace.write(_this.cls + " bufferingUpdate  '" + item.title + "' now has buffered: " + earliest + "s - " + latest + "s", audioplayer_types_1.notaAudioCategory);
            }
        };
        this._delegate.onFoundDuration = function (item, duration) {
            if (trace.isEnabled()) {
                trace.write(_this.cls + " found duration for '" + item.title + "': " + duration + "s", audioplayer_types_1.notaAudioCategory);
            }
        };
        this._delegate.onWillStartPlayingItem = function (item) {
            var _a;
            if (trace.isEnabled()) {
                trace.write(_this.cls + " will start playing '" + item.title + "'", audioplayer_types_1.notaAudioCategory);
            }
            if (item.artwork == null) {
                if (_this._cachedCover && _this._cachedCover.url === ((_a = _this.getMediaTrackForItem(item)) === null || _a === void 0 ? void 0 : _a.albumArtUrl)) {
                    if (trace.isEnabled()) {
                        trace.write(_this.cls + " got artwork from cache for '" + item.title + "'", audioplayer_types_1.notaAudioCategory);
                    }
                    item.artwork = _this._cachedCover.artwork;
                }
                else if (!_this._isRetrievingArtwork) {
                    _this.loadRemoteControlAlbumArtworkAsync(item);
                }
            }
        };
        this._delegate.onFinishedPlayingItem = function (item) {
            if (trace.isEnabled()) {
                trace.write(_this.cls + " finished playing '" + item.title + "'", audioplayer_types_1.notaAudioCategory);
            }
            var finishedIndex = _this._iosPlaylist.indexOfObject(item);
            _this._onEndOfTrackReached(finishedIndex);
            if (finishedIndex >= _this._iosPlaylist.count - 1) {
                _this._onEndOfPlaylistReached();
            }
        };
        this._delegate.onStateChanged = function (from, to) {
            _this._iosPlayerStateChanged(from, to);
        };
        this._delegate.onMetadataReceived = function (item, data) {
            var length = data.count;
            if (length > 0) {
                var title = '';
                var name_1 = '';
                var genre = '';
                for (var i = 0; i < length; i++) {
                    var entry = data[i];
                    switch (entry.commonKey) {
                        case 'title':
                            title = entry.stringValue;
                            break;
                        case 'name':
                            name_1 = entry.stringValue;
                            break;
                        case 'genre':
                            genre = entry.stringValue;
                            break;
                    }
                }
                if (title || name_1 || genre) {
                    _this._onMetadataReceived(title, name_1, genre);
                }
            }
        };
    };
    TNSAudioPlayer.prototype._onSleepTimerExpired = function () {
        var _this = this;
        var fadeTickMilliseconds = 250.0;
        var sleepTimerFadeDuration = 5000.0;
        var previousVolume = this._player.volume;
        var fadeInterval = setInterval(function () {
            var decreaseBy = fadeTickMilliseconds / sleepTimerFadeDuration;
            var newVolume = Math.max(_this._player.volume - decreaseBy, 0);
            _this._player.volume = newVolume;
            if (newVolume === 0) {
                if (trace.isEnabled()) {
                    trace.write(_this.cls + " - Volume faded out!", audioplayer_types_1.notaAudioCategory);
                }
                if (_this._player.state !== 2) {
                    if (trace.isEnabled()) {
                        trace.write(_this.cls + " - Volume faded out! - pausing", audioplayer_types_1.notaAudioCategory);
                    }
                    _this._player.pause();
                }
                _this._player.volume = previousVolume;
                clearInterval(fadeInterval);
                _super.prototype._onSleepTimerExpired.call(_this);
            }
        }, fadeTickMilliseconds);
    };
    TNSAudioPlayer.prototype._iosChangeTrackData = function (data) {
        var index = this._getCurrentPlaylistIndex();
        if (index !== null && this.playlist) {
            var currentTrack = this._iosPlaylist[index];
            var mediaTrack = this.getMediaTrackForItem(currentTrack);
            if (mediaTrack) {
                mediaTrack.title = data.title;
                mediaTrack.artist = data.artist;
                mediaTrack.album = data.album;
                mediaTrack.albumArtUrl = data.albumArtUrl;
                currentTrack.title = data.title;
                currentTrack.artist = data.artist;
                currentTrack.album = data.album;
                if (this._cachedCover && this._cachedCover.url === data.albumArtUrl) {
                    currentTrack.artwork = this._cachedCover.artwork;
                }
                else {
                    this.loadRemoteControlAlbumArtworkAsync(currentTrack);
                }
            }
        }
    };
    TNSAudioPlayer.prototype._iosSeekTo = function (milliseconds, adaptToSeekableRanges, beforeTolerance, afterTolerance, completionHandler) {
        var _this = this;
        if (adaptToSeekableRanges === void 0) { adaptToSeekableRanges = false; }
        if (beforeTolerance === void 0) { beforeTolerance = kCMTimeZero; }
        if (afterTolerance === void 0) { afterTolerance = kCMTimeZero; }
        if (trace.isEnabled()) {
            trace.write(this.cls + "._iosSeekTo(...) - seekTo: " + milliseconds + "ms (adaptsToSeekableRanges=" + adaptToSeekableRanges + ",hasCompletionHandler=" + !!completionHandler + ")", audioplayer_types_1.notaAudioCategory);
        }
        this._isSeeking = true;
        var seekToSeconds = milliseconds / 1000.0;
        this._player.seekToByAdaptingTimeToFitSeekableRangesToleranceBeforeToleranceAfterCompletionHandler(seekToSeconds, adaptToSeekableRanges, beforeTolerance, afterTolerance, function (completed) {
            if (trace.isEnabled()) {
                trace.write(_this.cls + "._iosSeekTo(...) - seekTo: " + milliseconds + "ms (adaptsToSeekableRanges=" + adaptToSeekableRanges + ",hasCompletionHandler=" + !!completionHandler + ") seek completed = " + completed, audioplayer_types_1.notaAudioCategory);
            }
            _this._isSeeking = false;
            if (completionHandler) {
                completionHandler(completed);
            }
        });
    };
    TNSAudioPlayer.prototype._iosSetPlayingState = function () {
        this._onPlaying();
    };
    TNSAudioPlayer.prototype._iosPlayerStateChanged = function (from, to) {
        if (trace.isEnabled()) {
            trace.write(this.cls + "._iosPlayerStateChanged(...) - stateChanged: " + from + " -> " + to, audioplayer_types_1.notaAudioCategory);
        }
        switch (to) {
            case 0: {
                this._onBuffering();
                break;
            }
            case 1: {
                this._iosSetPlayingState();
                if (this._queuedSeekTo) {
                    this._iosSeekTo(this._queuedSeekTo, false, kCMTimeZero, kCMTimeZero);
                    this._queuedSeekTo = undefined;
                }
                break;
            }
            case 2: {
                this._onPaused();
                break;
            }
            case 3: {
                this._onStopped();
                break;
            }
            case 4: {
                this._onWaitingForNetwork();
                break;
            }
            case 5: {
                this._onPlaybackError(this._player.failedError);
                break;
            }
            default: {
                trace.write(this.cls + "._iosPlayerStateChanged(track) - unknown stateChanged: " + from + " -> " + to, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
            }
        }
    };
    TNSAudioPlayer.prototype.makeAudioItemForMediaTrack = function (track) {
        try {
            if (trace.isEnabled()) {
                trace.write(this.cls + ".makeAudioItemForMediaTrack(" + JSON.stringify(track) + ")", audioplayer_types_1.notaAudioCategory);
            }
            var url = track.url.substr(0, 7) === 'file://' ? NSURL.fileURLWithPath(track.url.substr(7)) : NSURL.URLWithString(track.url);
            if (trace.isEnabled()) {
                trace.write(this.cls + ".makeAudioItemForMediaTrack(track) - URL: " + url, audioplayer_types_1.notaAudioCategory);
            }
            var audioItem = new AudioItem({
                highQualitySoundURL: null,
                mediumQualitySoundURL: url,
                lowQualitySoundURL: null,
            });
            if (trace.isEnabled()) {
                trace.write(this.cls + ".makeAudioItemForMediaTrack(track) - AudioItem: " + JSON.stringify(audioItem), audioplayer_types_1.notaAudioCategory);
            }
            audioItem.title = track.title;
            audioItem.artist = track.artist;
            audioItem.album = track.album;
            return audioItem;
        }
        catch (err) {
            trace.write(this.cls + ".makeAudioItemForMediaTrack(track) - Error: Failed to create AudioItem for MediaTrack: " + err, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
            return;
        }
    };
    TNSAudioPlayer.prototype.getMediaTrackForItem = function (item) {
        var _a, _b, _c;
        return _c = (_b = (_a = this.playlist) === null || _a === void 0 ? void 0 : _a.tracks) === null || _b === void 0 ? void 0 : _b[this.getIndexForItem(item)], (_c !== null && _c !== void 0 ? _c : null);
    };
    TNSAudioPlayer.prototype.getCurrentMediaTrack = function () {
        return this.getMediaTrackForItem(this._player.currentItem);
    };
    TNSAudioPlayer.prototype.loadRemoteControlAlbumArtworkAsync = function (item) {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function () {
            var artworkUrl, image, _d, artwork, err_1;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        artworkUrl = (_b = (_a = this.playlist) === null || _a === void 0 ? void 0 : _a.tracks) === null || _b === void 0 ? void 0 : _b[this.getIndexForItem(item)].albumArtUrl;
                        if (artworkUrl == null) {
                            return [2];
                        }
                        this._isRetrievingArtwork = true;
                        _e.label = 1;
                    case 1:
                        _e.trys.push([1, 5, 6, 7]);
                        if (!utils.isFileOrResourcePath(artworkUrl)) return [3, 2];
                        _d = image_source_1.ImageSource.fromFileOrResourceSync(artworkUrl);
                        return [3, 4];
                    case 2: return [4, image_source_1.ImageSource.fromUrl(artworkUrl)];
                    case 3:
                        _d = _e.sent();
                        _e.label = 4;
                    case 4:
                        image = _d;
                        if (((_c = this.getCurrentMediaTrack()) === null || _c === void 0 ? void 0 : _c.albumArtUrl) === artworkUrl) {
                            artwork = MPMediaItemArtwork.alloc().initWithImage(image.ios);
                            this._cachedCover = { url: artworkUrl, artwork: artwork };
                            item.artwork = artwork;
                        }
                        else {
                            if (trace.isEnabled()) {
                                trace.write(this.cls + ".loadRemoteControlAlbumArtworkAsync() - loadRemoteControlArtwork loaded, but current track was changed", audioplayer_types_1.notaAudioCategory);
                            }
                        }
                        return [3, 7];
                    case 5:
                        err_1 = _e.sent();
                        trace.write(this.cls + ".loadRemoteControlAlbumArtworkAsync() - loadRemoteControlArtwork error for url '" + artworkUrl + "': " + err_1, audioplayer_types_1.notaAudioCategory, trace.messageType.error);
                        return [3, 7];
                    case 6:
                        this._isRetrievingArtwork = false;
                        return [7];
                    case 7: return [2];
                }
            });
        });
    };
    TNSAudioPlayer.prototype._exitHandler = function () {
        this.destroy();
    };
    return TNSAudioPlayer;
}(audioplayer_common_1.CommonAudioPlayer));
exports.TNSAudioPlayer = TNSAudioPlayer;
function NSArrayFromItems(items) {
    var e_2, _a;
    var arr = NSMutableArray.alloc().init();
    try {
        for (var items_1 = __values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
            var item = items_1_1.value;
            arr.addObject(item);
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (items_1_1 && !items_1_1.done && (_a = items_1.return)) _a.call(items_1);
        }
        finally { if (e_2) throw e_2.error; }
    }
    return arr;
}
//# sourceMappingURL=audioplayer.ios.js.map