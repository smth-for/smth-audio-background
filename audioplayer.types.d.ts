export declare const notaAudioCategory = "NotaAudioPlayer";
import { EventData } from '@nativescript/core/data/observable';
export declare class MediaTrackData {
    constructor(title: string, artist: string, album: string, albumArtUrl: string);
    title: string;
    artist: string;
    album: string;
    albumArtUrl: string;
}
export declare class MediaTrack extends MediaTrackData {
    constructor(url: string, title: string, artist: string, album: string, albumArtUrl: string);
    url: string;
}
export declare class Playlist {
    UID: string;
    tracks: MediaTrack[];
    get length(): number;
    constructor(UID: string, ...tracks: MediaTrack[]);
}
export declare enum PlaybackEvent {
    Stopped = "Stopped",
    Buffering = "Buffering",
    Playing = "Playing",
    Paused = "Paused",
    EndOfTrackReached = "EndOfTrackReached",
    EndOfPlaylistReached = "EndOfPlaylistReached",
    EncounteredError = "EncounteredError",
    TimeChanged = "TimeChanged",
    SleepTimerChanged = "SleepTimerChanged",
    SleepTimerEnded = "SleepTimerEnded",
    WaitingForNetwork = "WaitingForNetwork",
    PlaybackSuspend = "PlaybackSuspend",
    ReceivedMetadata = "ReceivedMetadata"
}
export declare enum PlaybackSuspend {
    None = "none",
    FocusLoss = "focus_lost",
    Unknown = "unknown"
}
export interface PlaybackSuspendEventData extends EventData {
    eventName: 'PlaybackSuspend';
    reason: PlaybackSuspend;
}
export interface TimeChangedEventData extends EventData {
    eventName: 'TimeChanged';
    playlistIndex: number;
    currentTime: number;
    duration: number;
}
export interface MetadataReceivedEventData extends EventData {
    eventName: 'MetadataReceived';
    title?: string;
    name?: string;
    genre?: string;
}
export interface PlayingEventData extends EventData {
    eventName: 'Playing';
    playlistIndex: number;
    currentTime: number;
    duration: number;
}
export interface PausedEventData extends EventData {
    eventName: 'Paused';
    playlistIndex: number;
    currentTime: number;
    duration: number;
}
export interface StoppedEventData extends EventData {
    eventName: 'Stopped';
}
export interface BufferingEventData extends EventData {
    eventName: 'Buffering';
}
export interface WaitingForNetworkEventData extends EventData {
    eventName: 'WaitingForNetwork';
}
export interface PlaybackErrorEventData extends EventData {
    eventName: 'EncounteredError';
}
export interface SleepTimerChangedEventData extends EventData {
    eventName: 'SleepTimerChanged';
    remaining: number;
}
export interface EndOfTrackReachedEventData extends EventData {
    eventName: 'EndOfTrackReached';
    endedTrackIndex: number;
}
export interface EndOfPlaylistReachedEventData extends EventData {
    eventName: 'EndOfPlaylistReached';
}
export interface SleepTimerEndedEventData extends EventData {
    eventName: 'SleepTimerEnded';
}
export interface Config {
    maxNetworkRetryCount: number;
    requiredPrebufferSizeInSeconds: number;
}
export interface PlaybackEventListener {
    onPlaybackEvent(evt: PlaybackEvent, arg?: any): void;
}
